// ignore_for_file: unused_local_variable, unused_import

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:project_name/firebase_options.dart';
import 'package:project_name/screens/Public_Screen/SplashScreen/Splash_screen.dart';
import 'package:project_name/screens/Public_Screen/dashboardcricket/dashboardcricket_binding.dart';
import 'package:project_name/screens/Utills/Paths.dart';
import 'package:project_name/services/appConfing.dart';
import 'controller/controllers/controller_initializor.dart';
import 'screens/Utills/Routes.dart';
import 'package:firebase_core/firebase_core.dart';

import 'services/service_initializer.dart';

void main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
await Firebase.initializeApp(
  options: DefaultFirebaseOptions.android,
  );
  await initializeServices();
  SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
    ],
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Game',
      themeMode: ThemeMode.light,
      theme: AppConfig.themeData,
      getPages: Routes.routes,
      initialBinding: InitialBindings(),
      debugShowCheckedModeBanner: false,
      defaultTransition: Transition.fade,
      // opaqueRoute: Get.isOpaqueRouteDefault,
      // popGesture: Get.isPopGestureEnable,
      initialRoute: Paths. SplashScreen ,
      // home: SplashScreen(),
    );
  }
}
