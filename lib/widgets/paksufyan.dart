import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../screens/Public_Screen/Group/group_controller.dart';

class paksufyanwidget extends GetWidget<GroupControoler> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/munib.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'ALI AYAZ (c)\nAll Rounder',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 148,  Runs:1248,  Hg:57* (4/18)',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/mubalam.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'FAYYAN (vc)\nAll Rounder',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 248,  Runs:3198,  Hg:126* (4/9)',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/nekraj.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'OWAIS IRSHAD\nAll Rounder',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 118,  Runs:498,  Hg:54,  (3/24)',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/saman.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'TALHA AMIR\nLeft Handed Batsman',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 88,  Runs:798,  Hg:63*',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/aman.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'SHAHZEEL\nAll Rounder',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 22,  Runs:138,  Hg:43*, (2/32)',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/anzal.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'HAMZA AYAZ (wk)\n Left Handed Batsman',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 78,  Runs:648,  Hg:46',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/taha.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'ROSHAN\n Right Arm fasr Bowler',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 12,  Wckts:13,  Best:2/21',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/mosamlo.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'UMER GUL\nRight Handed Batsman',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 219,  Runs:970,  Hg:106',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/ushaoo.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'A.WASI\n Right Handed Batsman',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 42,  Runs:148,  Hg:40',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/munib.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'UMAIR\nRight Arm Fast Bowler',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 186,  Wckts:153,  Best:4/29',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/shureefd.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'SADIQ GHORI\nRight Handed Batsman',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 178,  Runs:608,  Hg:68',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/usaid.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'HAMZA TAJ\nAll Rounder',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 42,  Runs:278,  Hg:37* (3/36)',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/utraheel.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'SHOAIB\n Right Handed Batsman',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 74,  Runs:437,  Hg:57',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Color.fromRGBO(13, 47, 48, 1),
              border:
                  Border.all(color: Colors.black.withOpacity(0.4), width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image: AssetImage(
                    "assets/images/munib.jpg",
                  ),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'OWAIM\nRight Handed Batsman',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'Ings: 12,  Runs:51,  Hg:9',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellowAccent,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
        ),

        //   Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/kasfi.jpg",
        //   width: 30, height: 30,),
        //   ),
        // Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('FAYYAN\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('UMER HAJI\n Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('ALI (c)\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('AWAIS\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/aman.jpg",
        //   width: 30, height: 50),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('SHAHZEEL\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        // ),
        //  Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('TALHA AMIR\n left Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('HAMZA AYAZ\n left Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('HAMZA TAJ(wk)\n Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('ROSHAN\n  Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('UMAIR\n Right Arm Fast Bowler', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('SADIQ\n Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Anus naranja\n Left Arm leg Break', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('SHOAIB TAJ\n Right Arm leg Break', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('WASI\n Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),

        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin:
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('?????\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      ],
    );
  }
}
