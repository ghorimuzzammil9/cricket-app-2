import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../screens/Public_Screen/Live_Update/liveuodate_controller.dart';

class ScoreBoardWidget extends GetWidget<LiveupdateController> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: controller.tabControllerTwo,
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Table(textDirection: TextDirection.ltr,
                // defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
                // border:TableBorder.all(width: 2.0,color: Colors.red),
                children: [
                  TableRow(
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 238, 238, 238)),
                      children: [
                        Container(
                            margin: EdgeInsets.only(
                              top: 5,
                              left: 5,
                              bottom: 5,
                            ),
                            child: Text("BATTERS",
                                textScaleFactor: 1.1,
                                overflow: TextOverflow.ellipsis)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("R", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("B", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("4s", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("6s", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("SR", textScaleFactor: 1.1)),
                      ]),
                  TableRow(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color.fromARGB(255, 238, 238, 238))),
                      children: [
                        Container(
                            margin:
                                EdgeInsets.only(top: 5, left: 5, bottom: 5),
                            child: Text("AZZAM", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0.0", textScaleFactor: 1.1)),
                      ]),
                  TableRow(
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 234, 234, 234)),
                      children: [
                        Container(
                            margin: EdgeInsets.only(
                              top: 5,
                              left: 5,
                              bottom: 5,
                            ),
                            child: Text("BABOO", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0", textScaleFactor: 1.1)),
                        Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 5, left: Get.width * 0.1),
                            child: Text("0.0", textScaleFactor: 1.1)),
                      ]),
    
                  // TableRow(
                  //     children: [
                  //       Container(
                  //           margin: EdgeInsets.only(
                  //             top: 5,
                  //             left: 5,
                  //             bottom: 5,
                  //           ),
                  //           child: Text("HUNAIN",
                  //               textScaleFactor:
                  //                   1.1,
                  //               overflow:
                  //                   TextOverflow
                  //                       .ellipsis)),
                  //       Container(
                  //           margin: EdgeInsets.only(
                  //               top: 5,
                  //               bottom: 5,
                  //               left: Get.width *
                  //                   0.1),
                  //           child: Text("R",
                  //               textScaleFactor:
                  //                   1.1)),
                  //       Container(
                  //           margin: EdgeInsets.only(
                  //               top: 5,
                  //               bottom: 5,
                  //               left: Get.width *
                  //                   0.1),
                  //           child: Text("B",
                  //               textScaleFactor:
                  //                   1.1)),
                  //       Container(
                  //           margin: EdgeInsets.only(
                  //               top: 5,
                  //               bottom: 5,
                  //               left: Get.width *
                  //                   0.1),
                  //           child: Text("4s",
                  //               textScaleFactor:
                  //                   1.1)),
                  //       Container(
                  //           margin: EdgeInsets.only(
                  //               top: 5,
                  //               bottom: 5,
                  //               left: Get.width *
                  //                   0.1),
                  //           child: Text("6s",
                  //               textScaleFactor:
                  //                   1.1)),
                  //       Container(
                  //           margin: EdgeInsets.only(
                  //               top: 5,
                  //               bottom: 5,
                  //               left: Get.width *
                  //                   0.1),
                  //           child: Text("SR",
                  //               textScaleFactor:
                  //                   1.1)),
                  //     ]),
                ]),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text('EXTRAS'), Text('00  (wd, nb, b, lb)')],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Text('fall of wicket'),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Text('.....    ......   .....'),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Text('Yet to bat'),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                'HUNAIN(wc), ABDURRAHMAN, AMIN GHORI, KASHIF(C), AMAN, AZEEM, ABDUL GHANY, DHANY, SHAHEEM(wk),,',
                style: TextStyle(fontSize: 15, color: Colors.grey),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Table(children: [
                TableRow(
                    decoration: BoxDecoration(
                        color: Color.fromARGB(255, 234, 234, 234)),
                    children: [
                      Container(
                          margin: EdgeInsets.only(
                            top: 5,
                            left: 5,
                            bottom: 5,
                          ),
                          child: Text("Bolers", textScaleFactor: 1.1)),
                      Container(
                          margin: EdgeInsets.only(
                              top: 5, bottom: 5, left: Get.width * 0.1),
                          child: Text("0", textScaleFactor: 1.1)),
                      Container(
                          margin: EdgeInsets.only(
                              top: 5, bottom: 5, left: Get.width * 0.1),
                          child: Text("M", textScaleFactor: 1.1)),
                      Container(
                          margin: EdgeInsets.only(
                              top: 5, bottom: 5, left: Get.width * 0.1),
                          child: Text("R", textScaleFactor: 1.1)),
                      Container(
                          margin: EdgeInsets.only(
                              top: 5, bottom: 5, left: Get.width * 0.1),
                          child: Text("W", textScaleFactor: 1.1)),
                      Container(
                          margin: EdgeInsets.only(
                              top: 5, bottom: 5, left: Get.width * 0.1),
                          child: Text("Econ", textScaleFactor: 1.1)),
                    ]),
              ]),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Table(children: [
                TableRow(children: [
                  Container(
                      margin: EdgeInsets.only(
                        top: 5,
                        left: 5,
                        bottom: 5,
                      ),
                      child: Text("SOHAN", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("0", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("M", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("R", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("W", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("00", textScaleFactor: 1.1)),
                ]),
              ]),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Table(children: [
                TableRow(children: [
                  Container(
                      margin: EdgeInsets.only(
                        top: 5,
                        left: 5,
                        bottom: 5,
                      ),
                      child: Text("SAFEER", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("0", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("M", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("R", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("W", textScaleFactor: 1.1)),
                  Container(
                      margin: EdgeInsets.only(
                          top: 5, bottom: 5, left: Get.width * 0.1),
                      child: Text("00", textScaleFactor: 1.1)),
                ]),
              ]),
            ),
          ]),
          Column(
            children: [
 Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  child: Text('SAIFULLAH(c)\n    Right handed batsman')),
              Container(
                  margin: EdgeInsets.only(top: 7),
                  child: Text('MUZZAMMIL\n    Left handed batsman')),
              Container(
                  margin: EdgeInsets.only(top: 7),
                  child: Text('WASMIL\n    Right handed batsman')),
              Container(
                  margin: EdgeInsets.only(top: 7),
                  child: Text('JAWWAD\n    Right handed batsman')),
              Container(
                  margin: EdgeInsets.only(top: 7),
                  child: Text('HAMDAN\n    Right handed batsman')),
              Container(
                  margin: EdgeInsets.only(top: 7),
                  child: Text('KHUBAIB\n    Right handed batsman')),
              Container(
                  margin: EdgeInsets.only(top: 7),
                  child: Text('ZAEEM(wc)\n    Right handed batsman')),
              Container(
                  margin: EdgeInsets.only(top: 7),
                  child: Text('BILLAY\n    Right handed batsman')),
              Container(
                  margin: EdgeInsets.only(top: 7),
                  child: Text('SOHAN\n    Right handed batsman')),
              Container(
                  margin: EdgeInsets.only(top: 7),
                  child: Text('SAFEER\n    Right handed batsman')),
              Container(child: Text('UMER\n    Right handed batsman')),
            ],
          ),
          Container(
            margin: EdgeInsets.all(30),
            child: Text("REPORT"),
          ),
          Container(
            margin: EdgeInsets.all(30),
            child: Text("LIVE  STATUS"),
          ),
          Container(
            margin: EdgeInsets.all(30),
            child: Text("OVERSSS"),
          ),
          Container(
            margin: EdgeInsets.all(30),
            child: Text("PLAYING ELEVEN"),
          ),
          Container(
            margin: EdgeInsets.all(30),
            child: Text("NEWS............"),
          ),
          Container(
            margin: EdgeInsets.all(30),
            child: Text("PHOTOOOOSSS"),
          ),
          Container(
            margin: EdgeInsets.all(30),
            child: Text("SCORECARD"),
          ),
       
            ],
          ),
        ],
      ),
    );
  }
}
