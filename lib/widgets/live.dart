// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../screens/Public_Screen/Live_Update/liveuodate_controller.dart';

// class LiveBoardWidget extends GetWidget<LiveupdateController> {
//   @override
//   Widget build(BuildContext context) {
//     return SingleChildScrollView(
//       child: Column(
//         children: [
//           Table(
//             textDirection: TextDirection.ltr,
//             // defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
//             // border:TableBorder.all(width: 2.0,color: Colors.red),
//             children: [
//               TableRow(
//                   decoration:
//                       BoxDecoration(color: Color.fromARGB(255, 238, 238, 238)),
//                   children: [
//                     Container(
//                         margin: EdgeInsets.only(
//                           top: 5,
//                           left: 5,
//                           bottom: 5,
//                         ),
//                         child: Text("BATTERS",
//                             textScaleFactor: 1.1,
//                             overflow: TextOverflow.ellipsis)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("R", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("B", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("4s", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("6s", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("SR", textScaleFactor: 1.1)),
//                   ]),
//               TableRow(
//                   decoration: BoxDecoration(
//                       border:
//                           Border.all(color: Color.fromARGB(255, 238, 238, 238))),
//                   children: [
//                     Container(
//                         margin: EdgeInsets.only(top: 5, left: 5, bottom: 5),
//                         child: Text("AZZAM", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0.0", textScaleFactor: 1.1)),
//                   ]),
//               TableRow(children: [
//                 Container(
//                     margin: EdgeInsets.only(
//                       top: 5,
//                       left: 5,
//                       bottom: 5,
//                     ),
//                     child: Text("BABOO", textScaleFactor: 1.1)),
//                 Container(
//                     margin:
//                         EdgeInsets.only(top: 5, bottom: 5, left: Get.width * 0.1),
//                     child: Text("0", textScaleFactor: 1.1)),
//                 Container(
//                     margin:
//                         EdgeInsets.only(top: 5, bottom: 5, left: Get.width * 0.1),
//                     child: Text("0", textScaleFactor: 1.1)),
//                 Container(
//                     margin:
//                         EdgeInsets.only(top: 5, bottom: 5, left: Get.width * 0.1),
//                     child: Text("0", textScaleFactor: 1.1)),
//                 Container(
//                     margin:
//                         EdgeInsets.only(top: 5, bottom: 5, left: Get.width * 0.1),
//                     child: Text("0", textScaleFactor: 1.1)),
//                 Container(
//                     margin:
//                         EdgeInsets.only(top: 5, bottom: 5, left: Get.width * 0.1),
//                     child: Text("0.0", textScaleFactor: 1.1)),
//               ]),
//             ],
//           ),
//           Container(
//             height: Get.height * 0.03,
//             width: Get.width * 2,
//             margin: EdgeInsets.only(top: 20),
//             decoration: BoxDecoration(
//               border: Border.all(color: Color.fromARGB(255, 238, 238, 238)),
//             ),
//           ),
//           Container(
//             height: Get.height * 0.03,
//             width: Get.width * 2,
//             margin: EdgeInsets.only(top: 20),
//           ),
//           Table(
//             children: [
//               TableRow(
//                   decoration:
//                       BoxDecoration(color: Color.fromARGB(255, 234, 234, 234)),
//                   children: [
//                     Container(
//                         margin: EdgeInsets.only(
//                           top: 5,
//                           left: 5,
//                           bottom: 5,
//                         ),
//                         child: Text("BOWLERS",
//                             textScaleFactor: 1.1,
//                             overflow: TextOverflow.ellipsis)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("R", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("B", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("4s", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("6s", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("SR", textScaleFactor: 1.1)),
//                   ]),
//               TableRow(
//                   decoration: BoxDecoration(
//                       border:
//                           Border.all(color: Color.fromARGB(255, 238, 238, 238))),
//                   children: [
//                     Container(
//                         margin: EdgeInsets.only(
//                           top: 5,
//                           left: 5,
//                           bottom: 5,
//                         ),
//                         child: Text("SOHAN", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0", textScaleFactor: 1.1)),
//                     Container(
//                         margin: EdgeInsets.only(
//                             top: 5, bottom: 5, left: Get.width * 0.1),
//                         child: Text("0.0", textScaleFactor: 1.1)),
//                   ]),
//               TableRow(
//                 decoration: BoxDecoration(
//                     border:
//                         Border.all(color: Color.fromARGB(255, 238, 238, 238))),
//                 children: [
//                   Container(
//                       margin: EdgeInsets.only(
//                         top: 5,
//                         left: 5,
//                         bottom: 5,
//                       ),
//                       child: Text("SAFEER", textScaleFactor: 1.1)),
//                   Container(
//                       margin: EdgeInsets.only(
//                           top: 5, bottom: 5, left: Get.width * 0.1),
//                       child: Text("0", textScaleFactor: 1.1)),
//                   Container(
//                       margin: EdgeInsets.only(
//                           top: 5, bottom: 5, left: Get.width * 0.1),
//                       child: Text("0", textScaleFactor: 1.1)),
//                   Container(
//                       margin: EdgeInsets.only(
//                           top: 5, bottom: 5, left: Get.width * 0.1),
//                       child: Text("0", textScaleFactor: 1.1)),
//                   Container(
//                       margin: EdgeInsets.only(
//                           top: 5, bottom: 5, left: Get.width * 0.1),
//                       child: Text("0", textScaleFactor: 1.1)),
//                   Container(
//                       margin: EdgeInsets.only(
//                           top: 5, bottom: 5, left: Get.width * 0.1),
//                       child: Text("0.0", textScaleFactor: 1.1)),
//                 ],
//               ),
//             ],
//           ),
//           Container(
//             height: Get.height * 0.03,
//             width: Get.width * 2,
//             margin: EdgeInsets.only(top: 20),
//             decoration: BoxDecoration(
//               border: Border.all(color: Color.fromARGB(255, 238, 238, 238)),
//             ),
//           ),
//           Container(
//             height: Get.height * 0.03,
//             width: Get.width * 2,
//             margin: EdgeInsets.only(top: 20),
//             decoration: BoxDecoration(
//               border: Border.all(color: Color.fromARGB(255, 238, 238, 238)),
//             ),
//           ),
//           Container(
//             margin: EdgeInsets.only(top: 40),
//             height: 30,
//             decoration: BoxDecoration(
//                 border: Border.all(color: Color.fromARGB(255, 238, 238, 238))),
//             child: Center(
//               child: RichText(
//                 text: TextSpan(
//                     text: "P'SHIP",
//                     style: TextStyle(
//                         fontSize: 16,
//                         color: Colors.black,
//                         fontWeight: FontWeight.w800),
//                     children: <TextSpan>[
//                       const TextSpan(
//                         text: "  0(0.0)",
//                         style: TextStyle(
//                             fontSize: 14,
//                             color: Colors.black,
//                             fontWeight: FontWeight.w400),
//                       )
//                     ]),
//               ),
//             ),
//           ),
//           Container(
//             height: 30,
//             decoration: BoxDecoration(
//                 border: Border.all(color: Color.fromARGB(255, 238, 238, 238))),
//             margin: EdgeInsets.only(),
//             child: Center(
//               child: RichText(
//                 text: TextSpan(
//                     text: "Reviews Remaining:",
//                     style: TextStyle(
//                         fontSize: 16,
//                         color: Colors.black,
//                         fontWeight: FontWeight.w800),
//                     children: <TextSpan>[
//                       const TextSpan(
//                         text: " SA - 2 of 2,AUS - 2 of 2",
//                         style: TextStyle(
//                             fontSize: 14,
//                             color: Colors.black,
//                             fontWeight: FontWeight.w400),
//                       )
//                     ]),
//               ),
//             ),
//           ),
//           Row(children: [
//             Padding(
//               padding: const EdgeInsets.only(left: 5, top: 20),
//               child: Text(
//                 "1st",
//                 style: TextStyle(
//                     fontSize: 18,
//                     color: Colors.black,
//                     fontWeight: FontWeight.w500),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.only(left: 5, top: 20),
//               child: Container(
//                 padding: EdgeInsets.all(10),
//                 margin: EdgeInsets.only(right: 10),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5),
//                   color: Color.fromARGB(255, 207, 207, 207),
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.only(top: 20),
//               child: Container(
//                 padding: EdgeInsets.all(10),
//                 margin: EdgeInsets.only(right: 10),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5),
//                   color: Color.fromARGB(255, 207, 207, 207),
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.only(top: 20),
//               child: Container(
//                 padding: EdgeInsets.all(10),
//                 margin: EdgeInsets.only(right: 10),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5),
//                   color: Color.fromARGB(255, 207, 207, 207),
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.only(top: 20),
//               child: Container(
//                 padding: EdgeInsets.all(10),
//                 margin: EdgeInsets.only(right: 10),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5),
//                   color: Color.fromARGB(255, 207, 207, 207),
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.only(top: 20),
//               child: Container(
//                 padding: EdgeInsets.all(10),
//                 margin: EdgeInsets.only(right: 10),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5),
//                   color: Color.fromARGB(255, 207, 207, 207),
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.only(top: 20),
//               child: Container(
//                 padding: EdgeInsets.all(10),
//                 margin: EdgeInsets.only(right: 10),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5),
//                   color: Color.fromARGB(255, 207, 207, 207),
//                 ),
//               ),
//             )
//           ]),
//         ],
//       ),
//     );
//   }
// }
