// ignore_for_file: must_be_immutable


// class NEKSOFTWIDGET extends StatelessWidget {
//   dynamic item;
//   NEKSOFTWIDGET({this.item});
  

//   @override
//   Widget build(BuildContext context) {
//     return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
//       Container(
//         margin: EdgeInsets.only(top: 5),
//         padding: EdgeInsets.all(5),
//         decoration: BoxDecoration(
//             color: Color.fromRGBO(13, 47, 48, 1),
//             border: Border.all(color: Colors.black.withOpacity(0.4), width: 1),
//             borderRadius: BorderRadius.circular(10)),
//         child: Row(
//           children: [
//             ClipRRect(
//               borderRadius: BorderRadius.circular(60),
//               child: Image(
//                 image: AssetImage(
//                   item["imageUrl"],
//                 ),
//                 width: 80,
//                 height: 80,
//                 fit: BoxFit.fill,
//               ),
//             ),
//             SizedBox(
//               width: 15,
//             ),
//             Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Row(
//                   children: [
//                     Text(
//                       item["name"] ,
//                       style: TextStyle(
//                           fontSize: 16,
//                           color: Colors.white,
//                           fontWeight: FontWeight.w700),
//                     ),
//                     Text(
//                       item["isCaptain"] == true
//                           ? " (c)"
//                           : "" ,
//                       style: TextStyle(
//                           fontSize: 16,
//                           color: Colors.white,
//                           fontWeight: FontWeight.w700),
//                     ),
//                     Text(
//                       item["isViceCaptain"] == true
//                               ? " (vc)"
//                               : "" ,
//                       style: TextStyle(
//                           fontSize: 16,
//                           color: Colors.white,
//                           fontWeight: FontWeight.w700),
//                     ),
//                   ],
//                 ),
//                 Text(
//                   item["category"],
//                   style: TextStyle(
//                       fontSize: 16,
//                       color: Colors.white,
//                       fontWeight: FontWeight.w700),
//                 ),
//                 Text(
//                   'Ings:' +
//                       item["ings"].toString() +
//                       ' Runs:' +
//                       item["runs"].toString() +
//                       ' Hg:' +
//                       item["hg"].toString(),
//                   style: TextStyle(
//                       fontSize: 16,
//                       color: Colors.yellowAccent,
//                       fontWeight: FontWeight.w700),
//                 ),
//               ],
//             )
//           ],
//         ),
//       ),
//     ]);
//   }
// }
