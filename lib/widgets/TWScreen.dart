
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:project_name/screens/Public_Screen/Group/group_controller.dart';

class TWScreenWidget extends GetWidget<GroupControoler> {
  @override
  Widget build(BuildContext context) {

    return 
    
   Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          
          Container(padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/kasfi.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('KASHIF SESUDIA (c)\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 258,  Runs:1498,  Hg:67* (6/12)',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/mubalam.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('HUNAIN GULZAR (vc)\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 168,  Runs:1198,  Hg:76, (5/18)',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), Container
          (margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/aman.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('AMAN TAJ\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 71,  Runs:598,  Hg:69, (3/32)',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/saman.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('SAMAN\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 88,  Runs:498,  Hg:37*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/aman.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('ZEESHAN ZIA\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 42,  Runs:538,  Hg:43*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/anzal.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('ANZAL (wk)\n Right Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 118,  Runs:648,  Hg:46',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
             padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/taha.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('TAHA ZAKA\n Right Arm fasr Bowler', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 36,  Wckts:73,  Best:4/21',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
             padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/mosamlo.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('MOOSA\nRight Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 19,  Runs:70,  Hg:28*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
             padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/ushaoo.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('USAMA\n Right Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 12,  Runs:48,  Hg:20*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/munib.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('HAMMAD\nRight Arm Fast Bowler', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 186,  Wckts:123,  Best:4/29',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
             padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/shureefd.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('SHURAIF\nRight Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 28,  Runs:108,  Hg:38',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/usaid.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('USAID\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 12,  Runs:78,  Hg:17*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), 
          Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/utraheel.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('ITBAN\n Right Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 24,  Runs:137,  Hg:12*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), 
          Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/munib.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('ARBAB\nRight Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 60,  Runs:348,  Hg:27',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
          
          // Container(margin: EdgeInsets.only(top: 10,left: 10),
          //   child: Text('OWNER & CAPTAIN of TITANS WARRIURS\n     KASHIF SESUDIA',
          //   style: TextStyle(fontSize: 20, color: Colors.black,
          //   fontWeight: FontWeight.w700),),
          // ),
        
    
        //   Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/kasfi.jpg",
        //   width: 30, height: 30,),
        //   ),
        // Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Kashif Rafiq\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Abdullah Hyderabad\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Hunain Gulzar\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Azeem DGC\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/aman.jpg",
        //   width: 30, height: 50),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Aman\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        // ),
        //  Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Abdur Rahman\n Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ), 
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Azzam Gulsher\n Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Asghar Babo\n Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Shaheem\n  Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Huzaifah\n Right Arm Fast Bowler', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Amin Ghori\n Right Handed Batsman', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Khalid Rafiq\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Ishaq\n Right Arm Fast Bowler', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Dhani\n Left Arm Fast Bowler', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset(""),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('Yousuf Ghori\n All Rounder', style: TextStyle(fontSize: 10,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
    
        ],
      );
    
  }
  }