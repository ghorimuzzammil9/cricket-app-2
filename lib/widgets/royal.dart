import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../screens/Public_Screen/Group/group_controller.dart';
class royalwidget extends GetWidget<GroupControoler> {
  @override
  Widget build(BuildContext context) {

    return Container(padding: EdgeInsets.all(
      10
    ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          
      
          Container(padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/munib.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('MUNEEB  (c)\nRight Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 128,  Runs:1198,  Hg:87*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/mubalam.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('MUBEEN ALAM  (vc)\nRight Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 248,  Runs:3198,  Hg:136*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), Container
          (margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/nekraj.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('NEK RAJ\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 78,  Runs:698,  Hg:113',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/saman.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('SAMAN\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 88,  Runs:498,  Hg:37*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/aman.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('ZEESHAN ZIA\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 42,  Runs:538,  Hg:43*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/anzal.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('ANZAL (wk)\n Right Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 118,  Runs:648,  Hg:46',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
             padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/taha.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('TAHA ZAKA\n Right Arm fasr Bowler', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 36,  Wckts:73,  Best:4/21',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
             padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/mosamlo.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('MOOSA\nRight Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 19,  Runs:70,  Hg:28*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
             padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/ushaoo.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('USAMA\n Right Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 12,  Runs:48,  Hg:20*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/munib.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('HAMMAD\nRight Arm Fast Bowler', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 186,  Wckts:123,  Best:4/29',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
             padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/shureefd.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('SHURAIF\nRight Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 28,  Runs:108,  Hg:38',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
           Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/usaid.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('USAID\nAll Rounder', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 12,  Runs:78,  Hg:17*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), 
          Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/utraheel.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('ITBAN\n Right Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 24,  Runs:137,  Hg:12*',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ), 
          Container(
             margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: Color.fromRGBO(13, 47, 48, 1),
              border: Border.all(
                color: Colors.black.withOpacity(0.4),
                width: 1
              ),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              children: [
ClipRRect(
                borderRadius: BorderRadius.circular(60),
                child: Image(
                  image:AssetImage("assets/images/munib.jpg",
                  ),
                width: 80,height: 80,
                  fit: BoxFit.fill,
                ),
              ),
             SizedBox(
              width: 15,
             ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('ARBAB\nRight Handed Batsman', style: TextStyle(fontSize: 16,
              color: Colors.white, fontWeight: FontWeight.w700),
              ),
                  Text('Ings: 60,  Runs:348,  Hg:27',
                  style: TextStyle(fontSize: 16,
              color: Colors.yellowAccent, fontWeight: FontWeight.w700),),
                ],
              )
              ],
            ),
          ),
         
          // ListTile(
          //   leading: ClipRRect(
          //     borderRadius: BorderRadius.circular(60),
          //     child: Image(
          //       image:AssetImage("assets/images/munib.jpg",
          //       ),
          //     width: 80,height: 80,
          //       fit: BoxFit.fill,
          //     ),
          //   ),
          //   title:
          //   subtitle: 
          // ),
        
        
          //   Container(
             
          //     margin: EdgeInsets.only(top: 10,left: 10),
          // child: Image.asset("assets/images/munib.jpg",
          // fit: BoxFit.fill,
          //  width: 60,
          //     height:60,
          // ),
          // ),
          //  Container(margin: 
          // EdgeInsets.only(top: 10, left: 20),
          //   child: Text('MUNEEB (c)\n Right Handed Batsman', style: TextStyle(fontSize: 20,
          //   color: Colors.grey, fontWeight: FontWeight.w700),),
          // ),
      
        //   Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/mubalam.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60, ),
        //   ),
        // Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('MUBEEN ALAM (vc)\n All Rounder', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
      
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/nekraj.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('NEK RAJ\n All Rounder', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/saman.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('SAMAN\n All Rounder', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/aman.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('ZEESHAN ZIA\nAll Rounder', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/anzal.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        // ),
        //  Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('ANZAL (wk)\n Right Handed Batsman', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/taha.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ), 
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('TAHA\n Right Arm fasr Bowler', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/mosamlo.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('MOOSA\n Right Handed Batsman', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/ushaoo.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('USAMA\n  Right Handed Batsman', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //    Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('HAMMAD\n Right Arm Fast Bowler', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/shureefd.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('SHURAIF\n Right Handed Batsman', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/usaid.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('USAID\n All Rounder', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("assets/images/utraheel.jpg",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('ITBAN\n Right Handed Batsman', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //     Container(margin: EdgeInsets.only(top: 10,left: 10),
        //   child: Image.asset("",
        //    fit: BoxFit.fill,
        //    width: 60,
        //       height:60,),
        //   ),
        //    Container(margin: 
        //   EdgeInsets.only(top: 10, left: 20),
        //     child: Text('ARBAB\n Right Handed Batsman', style: TextStyle(fontSize: 20,
        //     color: Colors.grey, fontWeight: FontWeight.w700),),
        //   ),
      
        //   //  Container(margin: EdgeInsets.only(top: 10,left: 10),
          // child: Image.asset(""),
          // ),
          //  Container(margin: 
          // EdgeInsets.only(top: 10, left: 20),
          //   child: Text('Huzaifa\n All Rounder', style: TextStyle(fontSize: 10,
          //   color: Colors.grey, fontWeight: FontWeight.w700),),
          // ),
      
        ],
      ),
    );
  }
  }