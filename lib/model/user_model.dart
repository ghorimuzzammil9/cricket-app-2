class UserModel {
  String? email;
  String? fullName;
  String? phoneNumber;
  String? photoUrl;
  String? platform;
  String? type;
  String? uid;
  bool? isCompleted;
  String? playerCategary;
  String? matches;
  String? innings;
  String? totalRuns;
  String? totalWickets;
  String? highScore;
  String? bestBowlingFigers;
  String? batingAverage;
  String? bowlingEconomy;
  String? batingStrikerate;
  String? bowlingAverage;
    String? category;
  String? batingType;
  String? bowlingType;

  UserModel(
      {this.email,
      this.fullName,
      this.phoneNumber,
      this.photoUrl,
      this.platform,
      this.type,
      this.uid,
      this.isCompleted,
      this.playerCategary,
      this.matches,
      this.innings,
      this.totalRuns,
      this.totalWickets,
      this.highScore,
      this.bestBowlingFigers,
      this.batingAverage,
      this.bowlingEconomy,
      this.batingStrikerate,
      this.bowlingAverage,
      this.batingType,
      this.category,
      this.bowlingType
      });

  UserModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    fullName = json['fullName'];
    phoneNumber = json['phoneNumber'];
    photoUrl = json['photoUrl'];
    platform = json['platform'];
    type = json['type'];
    uid = json['uid'];
    isCompleted = json['isCompleted'];
    playerCategary = json['playerCategary'];
    matches = json['matches'];
    innings = json['innings'];
    totalRuns = json['totalRuns'];
    totalWickets = json['totalWickets'];
    highScore = json['highScore'];
    bestBowlingFigers = json['bestBowlingFigers'];
    batingAverage = json['batingAverage'];
    bowlingEconomy = json['bowlingEconomy'];
    batingStrikerate = json['batingStrikerate'];
    bowlingAverage = json['bowlingAverage'];
     bowlingType = json['bowlingType'];
    batingType = json['batingType'];
    category = json['category'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['fullName'] = this.fullName;
    data['phoneNumber'] = this.phoneNumber;
    data['photoUrl'] = this.photoUrl;
    data['platform'] = this.platform;
    data['type'] = this.type;
    data['uid'] = this.uid;
    data['isCompleted'] = this.isCompleted;
    data['playerCategary'] = this.playerCategary;
    data['matches'] = this.matches;
    data['innings'] = this.innings;
    data['totalRuns'] = this.totalRuns;
    data['totalWickets'] = this.totalWickets;
    data['highScore'] = this.highScore;
    data['bestBowlingFigers'] = this.bestBowlingFigers;
    data['batingAverage'] = this.batingAverage;
    data['bowlingEconomy'] = this.bowlingEconomy;
    data['batingStrikerate'] = this.batingStrikerate;
    data['bowlingAverage'] = this.bowlingAverage;
     data['bowlingType'] = this.bowlingType;
    data['batingType'] = this.batingType;
    data['category'] = this.category;
    return data;
  }
}
