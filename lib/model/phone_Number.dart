class PhoneNumberDTO {
  String? number;
  String? dialCode;

  PhoneNumberDTO({this.number, this.dialCode});

  PhoneNumberDTO.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    dialCode = json['dialCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this.number;
    data['dialCode'] = this.dialCode;
    return data;
  }
}