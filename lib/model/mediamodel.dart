class MediaModel {
  String? name;
  String? type;
  String? url;
  String? thumbnail;

  MediaModel({
    this.name,
    this.type,
    this.url,
    this.thumbnail,
  });

  static MediaModel fromJSON(Map<String, dynamic> json) {
    var media = new MediaModel(
        name: json["name"] ?? "",
        type: json["type"] ?? "",
        url: json["url"] ?? "",
        thumbnail: json["thumbnail"] ?? "");

    return media;
  }

  static Map<String, dynamic> toJSON(MediaModel media) {
    Map<String, dynamic> mediaJSON = {};
    mediaJSON["name"] = media.name;
    mediaJSON["type"] = media.type;
    mediaJSON["url"] = media.url;
    mediaJSON["thumbnail"] = media.thumbnail;

    return mediaJSON;
  }
}