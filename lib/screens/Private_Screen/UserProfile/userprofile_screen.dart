import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'userprofile_controller.dart';

class userProfileScreen extends GetView<UserProfileController>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color.fromRGBO(13, 48, 47, 1),
        title: Text('User Profile'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(14),
              height: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage('https://img1.hscicdn.com/image/upload/f_auto,t_ds_w_1200/lsci/db/PICTURES/CMS/86300/86316.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Center(
                child: CircleAvatar(
                  radius: 60,
                  backgroundImage: AssetImage('assets/images/DANI NIMMO.jpg'),
                ),
              ),
            ),
            SizedBox(height: 20),
            Text(
              'DANIYAL BILAL',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,fontStyle: FontStyle.italic
              ),
            ),
            Text(
              'All-Rounder / Left Arm Fast Bowler / Right-Arm Batsman',
              style: TextStyle(
                fontSize: 13,
                color: Colors.grey, fontWeight: FontWeight.w500
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                'About Me: Badamy Cricket Club, Debute 2010 againts Rajpot CC, Played khi & hyd. ',
                style: TextStyle(fontSize: 16),
              ),
            ),
            SizedBox(height: 20),
             _buildInfoRow('Matches', '102', 'Innings', '79'),
              _buildOtherInfo('Total Runs', '860', 'Total Wickets', '150'),
            _buildInfoRow('Best Batting Score', '67 (not out)', 'Best Bowling Figures', '5/11'),
              _buildOtherInfo('50/100', '02/00', '3+wkt', '13'),
               _buildInfoRow('Batting Average', '18.53', 'Bowling Economy', '4.5'),
              _buildInfoRow('Batting Strike Rate', '120', 'Bowling Strike Rate', '25'),
              
            // SizedBox(height: 20),
            // _buildOtherInfo('Total Runs', '1200', 'Total Wickets', '150'),
          ],
        ),
      ),
    );
  }

  Widget _buildInfoRow(String title1, String value1, String title2, String value2) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              Text(
                title1,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                value1,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              Text(
                title2,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                value2,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildOtherInfo(String title1, String value1, String title2, String value2) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              Text(
                title1,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                value1,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              Text(
                title2,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                value2,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
