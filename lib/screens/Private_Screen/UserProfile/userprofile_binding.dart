import 'package:get/get.dart';

import 'userprofile_controller.dart';

class UserProfileBinding extends Bindings{
  @override
  void dependencies (){
    Get.put(UserProfileController());
  
}}