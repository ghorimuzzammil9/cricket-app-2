import 'package:get/get.dart';
import 'package:project_name/screens/Private_Screen/setting/setting_controller.dart';

class SettingBinding extends Bindings{
  @override
  void dependencies (){
    Get.put(SettingController());
  
}}