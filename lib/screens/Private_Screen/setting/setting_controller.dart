import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/controller/controllers/user_controller.dart';
import 'package:project_name/model/user_model.dart';
import 'package:project_name/screens/Utills/Paths.dart';

import '../../../services/storage_service.dart';


class SettingController extends GetxController {
 UserModel? user;

   UserController userController=Get.find<UserController>();
    StorageService st=Get.find<StorageService>();
  logout () {
st. removeAllProperties();
    userController.removeAllProperties();
    Get.offAllNamed(Paths.LoginEmailScreen);

  }
     @override
  void onInit() {
    
    super.onInit();
    user = userController.user;
    update();
  }
   var setting = [
    {
      "title": "Create Users",
       "path": Paths.createUserScreen
    },
    {
      "title": "Delete Users",
      "path": Paths.deleteUsersScreen
    },
    {
      "title": "Create Team",
       "path": Paths.createteamScreen
    },
    {
     "title": "Delete Team",
     "path": Paths.deleteteamScreen
    },

    ];
  popap(BuildContext context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text("Are you sure you want to delete your account?"),
        actions: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: () {
                  // Add your NO button action here
                },
                child: Text(
                  'NO',
                  style: TextStyle(
                    color: Color.fromRGBO(13, 48, 47, 1), // Set the text color to black
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  // Add your YES button action here
                },
                child: Text(
                  'YES',
                  style: TextStyle(
                    color: Color.fromRGBO(13, 48, 47, 1), // Set the text color to black
                  ),
                ),
              ),
            ],
          )
        ],
      );
    },
  );
}

  
}