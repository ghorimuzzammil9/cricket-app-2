import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/Utills/Paths.dart';
import 'setting_controller.dart';

class SettingScreen extends GetView<SettingController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SettingController>(
      builder: (controller) {
        return Scaffold(
          backgroundColor: Color.fromRGBO(234, 235, 236, 1),
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(13, 48, 47, 1),
            automaticallyImplyLeading: true,
            leading: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Icon(
                Icons.arrow_back_ios,
                size: 20,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
            title: Text(
              "Settings",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
               SizedBox(height: 10,),
                InkWell(
                  onTap: () => Get.toNamed(Paths.MainProfileScreen),
                  child: Container(
                    margin: EdgeInsets.only(left: 10, right: 10, top: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          blurRadius: 30,

                          offset: Offset(1, 1), // Shadow position
                        ),
                      ],
                      color: Color.fromARGB(255, 255, 255, 255),
                    ),
                    padding: EdgeInsets.only(
                        top: 10, bottom: 10, left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 40, width: 40,
                          // decoration: BoxDecoration(
                          //   borderRadius: BorderRadius.circular(70),
                          //   border: Border.all(color: Colors.black)
                          // ),
                          margin: EdgeInsets.only(left: 10),
                          // child: Padding(padding: EdgeInsets.all(2),

                          child: CircleAvatar(
                            radius: 70,
                            backgroundImage: controller.user!.photoUrl == null
                                ? NetworkImage(
                                    'https://templates.joomla-monster.com/joomla30/jm-news-portal/components/com_djclassifieds/assets/images/default_profile.png')
                                : NetworkImage(controller.user!.photoUrl!),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 40),
                          child: Text(
                            '${controller.user!.fullName!}',
                            style: TextStyle(
                              fontSize: 18,
                              // fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(0, 0, 0, 1),
                            ),
                          ),
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Color.fromARGB(255, 0, 0, 0),
                          size: 18,
                        ),
                      ],
                    ),
                  ),
                ),
               SizedBox(height: 10,),
                ListView.builder(
                  itemCount: controller.setting.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 20, left: 15, right: 15),
                          child: GestureDetector(
                            onTap: () {
                              Get.toNamed(controller.setting[index]["path"]!);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  controller.setting[index]["title"].toString(),
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromRGBO(0, 0, 0, 1),
                                  ),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                  size: 18,
                                ),
                              ],
                            ),
                          ),
                        ),
                        Divider(
                          // Add a divider
                          color: Color.fromARGB(
                              255, 170, 170, 170), // Set the divider color
                          thickness: 1.0, // Set the thickness of the divider
                        ),
                      ],
                    );
                  },
                ),
                Container(
                  margin: EdgeInsets.only(top: 320, left: 20, right: 20),
                  child: ElevatedButton(
                    child: Text(
                      'Logout',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      fixedSize: Size(Get.width, 50),
                      backgroundColor: Color.fromRGBO(13, 48, 47, 1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    onPressed: () {
                      controller.logout();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: ElevatedButton(
                    child: Text(
                      'Delete Account',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      fixedSize: Size(Get.width, 50),
                      backgroundColor: Color.fromRGBO(13, 48, 47, 1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    onPressed: () {
                      controller.popap(context);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
