import 'package:get/get.dart';
import 'package:project_name/controller/controllers/user_controller.dart';
import 'package:project_name/model/user_model.dart';

class MainProfileController extends GetxController {

  UserController userController=Get.find<UserController>();
  UserModel ?user;

  @override
  void onInit() {
    
    user=userController.user;
    update();
    super.onInit();
  }
}