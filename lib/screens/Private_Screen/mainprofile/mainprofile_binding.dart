import 'package:get/get.dart';
import 'package:project_name/screens/Private_Screen/mainprofile/mainprofile_controller.dart';

class MainProfileBinding extends Bindings{
  @override
  void dependencies (){
    Get.put(MainProfileController());
  
}}