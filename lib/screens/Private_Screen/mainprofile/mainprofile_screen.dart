


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/Private_Screen/mainprofile/mainprofile_controller.dart';
import 'package:project_name/screens/Utills/Paths.dart';

class MainProfileScreen extends GetView<MainProfileController>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () => Get.back(),
          child: Icon(Icons.arrow_back_ios)),
        backgroundColor:  Color.fromRGBO(13, 48, 47, 1),
        title: Text('Player Profile'),
        actions: [InkWell(
          onTap: () => Get.toNamed(Paths.MyProfileScreen),
          child: Icon(Icons.more_vert_outlined))],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(14),
              height: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage('https://img1.hscicdn.com/image/upload/f_auto,t_ds_w_1200/lsci/db/PICTURES/CMS/86300/86316.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Center(
                child: CircleAvatar(
                  radius: 60,
                  backgroundImage:  controller.user!.photoUrl==null?  NetworkImage( 'https://templates.joomla-monster.com/joomla30/jm-news-portal/components/com_djclassifieds/assets/images/default_profile.png'):

                NetworkImage(controller.user!.photoUrl!),
                ),
              ),
            ),
            SizedBox(height: 20),
            Text(
              '${controller.user!.fullName}',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,fontStyle: FontStyle.italic
              ),
            ),
            Text(
              'All-Rounder / Left Arm Fast Bowler / Right-Arm Batsman',
              style: TextStyle(
                fontSize: 13,
                color: Colors.grey, fontWeight: FontWeight.w500
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                'About Me: Badamy Cricket Club, Debute 2010 againts Rajpot CC, Played khi & hyd. ',
                style: TextStyle(fontSize: 16),
              ),
            ),
            SizedBox(height: 20),
             _buildInfoRow('Matches', '102', 'Innings', '79'),
              _buildOtherInfo('Total Runs', '860', 'Total Wickets', '150'),
            _buildInfoRow('Best Batting Score', '67 (not out)', 'Best Bowling Figures', '5/11'),
              _buildOtherInfo('50/100', '02/00', '3+wkt', '13'),
               _buildInfoRow('Batting Average', '18.53', 'Bowling Economy', '4.5'),
              _buildInfoRow('Batting Strike Rate', '120', 'Bowling Strike Rate', '25'),
              
            // SizedBox(height: 20),
            // _buildOtherInfo('Total Runs', '1200', 'Total Wickets', '150'),
          ],
        ),
      ),
    );
  }

  Widget _buildInfoRow(String title1, String value1, String title2, String value2) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              Text(
                title1,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                value1,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              Text(
                title2,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                value2,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildOtherInfo(String title1, String value1, String title2, String value2) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              Text(
                title1,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                value1,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              Text(
                title2,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                value2,
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'mainprofile_controller.dart';


// class MainProfileScreen extends GetView<MainProfileController> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       // appBar: AppBar(
//       //   backgroundColor:Colors.white,
//       //   elevation: 0,
//       //   scrolledUnderElevation: 0,
//       //   centerTitle: true,
//       //   leading: IconButton(
//       //     icon: Icon(
//       //       Icons.arrow_back_ios,
//       //       color: Colors.black,
//       //       size: 18,
//       //     ),
//       //     onPressed: () {
//       //       Get.back();
//       //     },
//       //   ),
//       //   // title: Text(
//       //   //   "My Profile",
//       //   //   style: TextStyle(
//       //   //       fontSize: 18,
//       //   //       fontWeight: FontWeight.w500,
//       //   //       color: Color.fromRGBO(255, 255, 255, 1)),
//       //   // ),
//       //   actions: [
//       //     Container(
//       //       margin: EdgeInsets.only(right: 16),
//       //       child: GestureDetector(
//       //         onTap: () {
              
//       //         },
//       //         child: Image.asset(
//       //           'assets/images/DANI NIMMO.jpg',
//       //           height: 22,
//       //           width: 30,
//       //         ),
//       //       ),
//       //     ),
//       //   ],
//       // ),
       
//        body: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Stack(
//               children: [
//                 Container(
//                   height: Get.height * 0.25, width: Get.width,
//                   // margin: EdgeInsets.only(top:10),
//                   decoration: BoxDecoration(
//                     image: DecorationImage(
//                         image: AssetImage(
//                           "assets/images/stats.jpg",
//                         ),
//                         fit: BoxFit.cover),
//                   ),
//                   child: Container(
//                     margin: EdgeInsets.only(left: 10, right: 10, bottom: 30),
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Icon(
//                           Icons.arrow_back,
//                           size: 30,
//                           color: Colors.white,
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                           children: [
//                             // Image.asset(
//                             //   "assets/images/DANI NIMMO.jpg",
//                             //   width: 30,
//                             // ),
//                             Icon(
//                               Icons.more_vert,
//                               size: 30,
//                               color: Colors.white,
//                             ),
//                           ],
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//                 Center(
//                     child:  Positioned(
//                       child: Container(
//                         decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(70),
//                         ),
//                         margin: EdgeInsets.only(top: 160),
//                         child: Image.asset("assets/images/DANI NIMMO.jpg",
//                             width: Get.width * 0.2),
//                       ),
//                     )),
//               ],
//             ),
//             Container(
//               margin: EdgeInsets.only(top: 10, left: 15),
//               child: Center(
//                 child: Text("DANIYAL BILAL", style: TextStyle(
//                   fontSize: 20, color: Colors.black
//                 ),),
//               ),
//             ),
//             Container(
//               margin: EdgeInsets.only(top: 10, left: 10),
//               child: Text('He was born in Karachi,South',
//               style: TextStyle(fontSize: 15, color: Colors.black),),
//             ),
//             Container(
//               margin: EdgeInsets.only(top: 2, left: 10),
//               child: Text('Debute 2010 Against Rajpot. He played in Badamy CC, ASPL,',
//               style: TextStyle(fontSize: 15, color: Colors.black),),
//             ),
//            Container(
//             margin: EdgeInsets.only(top: 10, left: 10),
//             child: Text("Left Handed Bowler\n Right Handed Batsmen",
//             style: TextStyle(fontSize: 13, color: Colors.black),),
//            ),
           
//            Container(
//             margin: EdgeInsets.only(top: 10, left: 10),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Container(
//                   margin: EdgeInsets.only(bottom: 5, left: 5),
//                   child: Text("Bating Career", style: TextStyle(fontSize: 13, color: Colors.grey), )),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: [
//                     Text("Matches", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                        Text("Inn", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                           Text("Runs", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                              Text("Hg", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                                 Text("Avg", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                   ],
//                 ),
//                 Container(
//                   margin: EdgeInsets.only(top: 10),
//                   child:  Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: [
//                     Text("    105", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                        Text("   78", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                           Text("679", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                              Text("78", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                                 Text("13", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                   ],
//                 ),
//                 ),
//                  Container(
//             margin: EdgeInsets.only(top: 10, left: 10),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Container(
//                   margin: EdgeInsets.only(bottom: 5),
//                   child: Text("Bowling Career", style: TextStyle(fontSize: 13, color: Colors.grey), )),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: [
//                     Text("Matches", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                        Text("Inn", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                           Text("Wtks", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                              Text("Best", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                                 Text("Avg", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                   ],
//                 ),
//                 Container(
//                   margin: EdgeInsets.only(top: 10),
//                   child:  Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: [
//                     Text("     105", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                        Text("   105", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                           Text("130", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                              Text(  "5/7", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                                 Text("4.57", style: TextStyle(fontSize: 10, color: Colors.grey),),
//                   ],
//                 ),
//                 ),
          
//               ],
//             ),
//            ),
         
//         ],
//        ),



//     //   body: Container(
//     //     padding: EdgeInsets.all(10),
//     //     margin: EdgeInsets.only(top: Get.height * 0.04),
//     //     child: Column(
//     //       children: [
//     //         Row(
//     //           mainAxisAlignment: MainAxisAlignment.center,
//     //           crossAxisAlignment: CrossAxisAlignment.center,
//     //           children: [
//     //             Stack(
//     //               clipBehavior: Clip.none,
//     //               children: [
//     //                 Container(
//     //                   decoration: BoxDecoration(
//     //                     borderRadius: BorderRadius.circular(70),
//     //                     color: Colors.white,
//     //                     boxShadow: [
//     //                       BoxShadow(
//     //                         color: Colors.grey.withOpacity(0.2),
//     //                         spreadRadius: 6,
//     //                         blurRadius: 3,
//     //                         // offset: Offset(0, 7), // changes position of shadow
//     //                       ),
//     //                     ],
//     //                   ),
//     //                   // child: CircleAvatar(
//     //                   //   backgroundImage: AssetImage(
//     //                   //     'assets/images/.png',
//     //                   //   ),
//     //                   //   radius: 60,
//     //                   // ),
//     //                 ),
                    


//     //               ],
//     //             ),
//     //           ],
//     //         ),
//     //         Container(
//     //           margin: EdgeInsets.only(top: 18),
//     //           child: Text(
//     //             "CricketScoreApp",
//     //             style: TextStyle(
//     //                 fontSize: 24,
//     //                 color: Color.fromRGBO(69, 79, 99, 1),
//     //                 fontWeight: FontWeight.w600),
//     //           ),
//     //         ),
//     //          Form(
                         
//     //                       child: Column(
//     //                         children: [
//     //                           TextFormField(
//     //                             validator: ((value) {
//     //                               if (value == null || value.isEmpty) {
//     //                                 return "enterYourPhoneNumber".tr;
//     //                               }
//     //                               return null;
//     //                             }),
//     //                             keyboardType:
//     //                                 TextInputType.numberWithOptions(signed: true),
//     //                             // inputFormatters: [
//     //                             //   FilteringTextInputFormatter.digitsOnly,
//     //                             // ],
                          
//     //                             decoration: InputDecoration(
//     //                                 prefixIcon: Row(
//     //                                   mainAxisSize: MainAxisSize.min,
//     //                                   crossAxisAlignment:
//     //                                       CrossAxisAlignment.center,
//     //                                   children: [
                                      
//     //                                     // Container(
//     //                                     //   padding: EdgeInsets.only(right: 5),
//     //                                     //   child: Text(
//     //                                     //     // controller.dialCode,
//     //                                     //     style: TextStyle(
//     //                                     //       fontWeight: FontWeight.w500,
//     //                                     //       fontSize: 16,
//     //                                     //     ),
//     //                                     //   ),
//     //                                     // )
//     //                                   ],
//     //                                 ),
//     //                                 // prefixIcon: Transform.scale(
//     //                                 //   scale: 0.5,
//     //                                 //   child: Image.asset(
//     //                                 //     "assets/images/telephone.png",
//     //                                 //   ),
//     //                                 // ),
//     //                                 hintText: "Enter our Number".tr,
//     //                                 hintStyle: TextStyle(
//     //                                     fontWeight: FontWeight.w400,
//     //                                     fontSize: 12.0)),
//     //                           ),
                              
//     //                         ],
//     //                       ),
//     //                     ),
//     //                     Form(
                         
//     //                       child: Column(
//     //                         children: [
//     //                           TextFormField(
//     //                             validator: ((value) {
//     //                               if (value == null || value.isEmpty) {
//     //                                 return "enterYourPhoneNumber".tr;
//     //                               }
//     //                               return null;
//     //                             }),
                                
//     //                             keyboardType:
//     //                                 TextInputType.numberWithOptions(signed: true),
//     //                             // inputFormatters: [
//     //                             //   FilteringTextInputFormatter.digitsOnly,
//     //                             // ],
                          
//     //                             decoration: InputDecoration(
//     //                                 prefixIcon: Row(
//     //                                   mainAxisSize: MainAxisSize.min,
//     //                                   crossAxisAlignment:
//     //                                       CrossAxisAlignment.center,
//     //                                   children: [
                                      
//     //                                     // Container(
//     //                                     //   padding: EdgeInsets.only(right: 5),
//     //                                     //   child: Text(
//     //                                     //     // controller.dialCode,
//     //                                     //     style: TextStyle(
//     //                                     //       fontWeight: FontWeight.w500,
//     //                                     //       fontSize: 16,
//     //                                     //     ),
//     //                                     //   ),
//     //                                     // )
//     //                                   ],
//     //                                 ),
//     //                                 // prefixIcon: Transform.scale(
//     //                                 //   scale: 0.5,
//     //                                 //   child: Image.asset(
//     //                                 //     "assets/images/telephone.png",
//     //                                 //   ),
//     //                                 // ),
//     //                                 hintText: "Full Name".tr,
//     //                                 hintStyle: TextStyle(
//     //                                     fontWeight: FontWeight.w400,
//     //                                     fontSize: 12.0)),
//     //                           ),
                              
//     //                         ],
//     //                       ),
//     //                     ),
//     //       ],
//     //     ),
//     //   ),
//         )]));
//   }
// }
