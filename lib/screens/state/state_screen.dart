import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/state/state_controller.dart';


class StateScreen extends GetView {
  @override
  
  Widget build(BuildContext context) {
    SingleChildScrollView();
    return GetBuilder<StateController>(
      builder: (controller) {
        return Scaffold(  
          backgroundColor: Color.fromRGBO(255, 255, 255, 1),
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(13, 48, 47, 1),
            automaticallyImplyLeading: true,
            title: Text(
              "Player Stats",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
          ),
          
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                 decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(2),
    boxShadow: [
      BoxShadow(
        color: Colors.black.withOpacity(0.2),
        blurRadius: 30,
        
        offset: Offset(1,1), // Shadow position
      ),
    ],
  ),
                padding: const EdgeInsets.only(
                    top: 10, bottom: 10, left: 10, right: 10),
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  color: Color.fromARGB(255, 255, 255, 255),
                  alignment: Alignment.topLeft,
                  child: Container(
                    width: 40,
                    height: 40,
                    child: TextField(
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.search,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 20,
                  bottom: 40,
                 
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          // marginOnly(right: 20),
                          SizedBox(
                            width: 250,
                            child: Text(
                              'PLAYER LIST',
                              style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.w400,
                                  
                                  color: Color.fromARGB(255, 177, 177, 177)),
                            ),
                          ),
                          SizedBox(
                            child: Icon(
                              Icons.manage_accounts_outlined,
                              size: 80,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(color: Colors.black,),
                    ListView.separated(
                      shrinkWrap: true,
                      itemCount: controller.MediaItems.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          // margin: EdgeInsets.only(bottom: 0, top: 20),
                          // padding: EdgeInsets.only(
                          //   bottom: 10,
                          //   top: 0,
                          // ),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      controller.MediaItems[index]["text"],
                                       style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                    ),
                                  ),
                                  Container(
                                    width: 80,
                                    child: Image(
                                      image: AssetImage(
                                        "assets/images/pancil.png",
                                      ),
                                      width: Get.width,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      }, separatorBuilder: (BuildContext context, int index) { 
                        return Divider(
                          color: Colors.black,
                        );
                       },
                    ),
                    Divider(
                        color: Colors.black,
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
