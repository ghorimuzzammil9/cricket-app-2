import 'package:get/get.dart';
import 'package:project_name/screens/state/state_controller.dart';
class StateBinding extends Bindings{
 @override
  void dependencies(){
    Get.put(StateController());

  } 
}