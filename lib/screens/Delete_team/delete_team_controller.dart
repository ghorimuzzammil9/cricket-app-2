import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import '../../../model/user_model.dart';

class DeleteteamControoler extends GetxController {
  List<UserModel> users = [];

  late StreamSubscription postStream;

  @override
  void onInit() {
    
    super.onInit();
    initializeFeed();
  }

  var GroupAList = [
    {
      "title": "TITANS WARRIURS",
      "image": "assets/images/tw.jpeg",
    },
    {
      "name": "BABAR & SONS CONST",
      "image": "",
    },
    {
      "name": "NEKSOFT",
      "image": "",
    },
    {
      "name": "PAK SUFYAN STARS",
      "image": "",
    },
    {
      "name": "KN BUILDERS KING",
      "image": "",
    },
  ];

  List GroupBList = [
    {
      "name": "BADAMI STARS",
      "image": "",
    },
    {
      "name": "FRONT CC",
      "image": "",
    },
    {
      "name": "JAMIL SARWA SULTAN",
      "image": "",
    },
    {
      "name": "CHANDIO ROYALS",
      "image": "",
    },
    {
      "name": "BANGI LINE SUPER STAR",
      "image": "",
    },
  ];

  initializeFeed() {
    try {
      postStream = FirebaseFirestore.instance
          .collection('users')
          .snapshots()
          .listen((event) {
        for (var doc in event.docChanges) {
          var querySnapshot = doc.doc.data();
          var ind = users.indexWhere(
              (element) => element.email == querySnapshot!['email']);
          if (ind == -1) {
            users.add(UserModel.fromJson(querySnapshot!));
          }
        }
        update();

        update();
      });
    } catch (e) {
      print(e);
    }
  }
//     popap(BuildContext context) {
//   return showDialog(
//     context: context,
//     builder: (BuildContext context) {
//       return AlertDialog(
//         content: Text("Are you sure you want to delete your account?"),
//         actions: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.end,
//             children: [
//               TextButton(
//                 onPressed: () {
//                   // Add your NO button action here
//                 },
//                 child: Text(
//                   'NO',
//                   style: TextStyle(
//                     color: Color.fromRGBO(13, 48, 47, 1), // Set the text color to black
//                   ),
//                 ),
//               ),
//               TextButton(
//                 onPressed: () {
//           deleteUser(controller.users[index].uid!);
//                 },
//                 child: Text(
//                   'YES',
//                   style: TextStyle(
//                     color: Color.fromRGBO(13, 48, 47, 1), // Set the text color to black
//                   ),
//                 ),
//               ),
//             ],
//           )
//         ],
//       );
//     },
//   );
// }

}
