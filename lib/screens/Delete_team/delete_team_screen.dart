import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/Delete_team/delete_team_controller.dart';

class DeleteteamScreen extends GetView<DeleteteamControoler> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DeleteteamControoler>(builder: (controller) {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(13, 48, 47, 1),
            title: Text(
              "Delete Team",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
            centerTitle: true,
            leading: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Icon(
                Icons.arrow_back_ios,
                size: 20,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
          ),
          backgroundColor: Colors.white,
          body: StreamBuilder(
            stream: FirebaseFirestore.instance.collection('users').snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text('Error: ${snapshot.error}'),
                );
              }

              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              return ListView.builder(
                  itemCount: controller.users.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    // children: snapshot.data!.docs.map((DocumentSnapshot document) {
                    //   Map<String, dynamic> data = document.data() as Map<String, dynamic>;
                    return Column(
                      children: [
                        ListTile(
                          onTap: (() {
                            // controller.popap(context);
                          }),
                          leading: Container(
                            height: 40,
                            width: 40,
                            child: ClipOval(
                              child: Image.network(
                                controller.users[index].photoUrl == null
                                    ? 'https://templates.joomla-monster.com/joomla30/jm-news-portal/components/com_djclassifieds/assets/images/default_profile.png'
                                    : controller.users[index].photoUrl
                                        .toString(),
                                fit: BoxFit
                                    .cover, // Image ko oval me fit karne ke liye
                                width: 40,
                                height: 40,
                              ),
                            ),
                          ),
                          title: Text("${controller.users[index].fullName}"),
                          subtitle: Text("${controller.users[index].email}"),
                          trailing: (Icon(
                            Icons.delete,
                            color: Colors.red,
                            size: 30,
                          )),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 8, bottom: 8),
                            child: Divider(
                              thickness: 1,
                            ))
                      ],
                    );
                  });
            },
          ));
    });

    // GestureDetector(
    //   onTap: () {
    //     Get.toNamed(Paths.Scorebookscreen);
    //   },
    //   child: Container(
    //     padding: EdgeInsets.only(left: 10, right: 10),
    //     child: Column(
    //       children: [
    //         SizedBox(
    //           height: 15,
    //         ),
    //         Row(
    //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //           children: [
    //             Text(
    //               "Group A",
    //               style: TextStyle(
    //                   fontSize: 25,
    //                   color: Colors.black,
    //                   fontWeight: FontWeight.w800),
    //             ),
    //             Text(
    //               "Group B",
    //               style: TextStyle(
    //                   fontSize: 25,
    //                   color: Colors.black,
    //                   fontWeight: FontWeight.w800),
    //             ),
    //           ],
    //         ),
    //         SizedBox(
    //           height: 15,
    //         ),
    //         Row(
    //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //           children: [
    //             ListView.builder(
    //               itemCount: controller.GroupAList.length,
    //               itemBuilder: (BuildContext context, int index) {
    //                 return InkWell(
    //                   onTap: () => Get.toNamed(
    //                     Paths.GroupdetailScreen,
    //                     arguments: {
    //                       "title": "TITANS WARRIORS",
    //                     },
    //                   ),
    //                   child: Image.asset(
    //                     "assets/images/tw.jpeg",
    //                     height: 100,
    //                     width: 100,
    //                     fit: BoxFit.fill,
    //                   ),
    //                 );
    //               },
    //             ),
    //             Expanded(
    //               child: ListView.builder(
    //                 itemCount: controller.GroupBList.length,
    //                 itemBuilder: (BuildContext context, int index) {
    //                   return Column(
    //                     children: [
    //                       Text(
    //                         controller.GroupBList[index]["name"]
    //                             .toString(),
    //                       ),
    //                       Text(controller.GroupBList[index]["model"]
    //                           .toString())
    //                     ],
    //                   );
    //                 },
    //               ),
    //             ),

    //             // Column(
    //             //   mainAxisAlignment: MainAxisAlignment.start,
    //             //   crossAxisAlignment: CrossAxisAlignment.start,
    //             //   children: [

    //             //     SizedBox(
    //             //       height: 05,
    //             //     ),
    //             //     InkWell(
    //             //       onTap: () => Get.toNamed(
    //             //         Paths.GroupdetailScreen,
    //             //         arguments: {
    //             //           "title": "BABAR & SONS CONST",
    //             //         },
    //             //       ),
    //             //       child: Image.asset(
    //             //         "assets/images/Babar&Sons.jpeg",
    //             //         height: 100,
    //             //         width: 100,
    //             //         fit: BoxFit.fill,
    //             //       ),
    //             //     ),
    //             //     SizedBox(
    //             //       height: 05,
    //             //     ),
    //             //     InkWell(
    //             //       onTap: () => Get.toNamed(
    //             //         Paths.GroupdetailScreen,
    //             //         arguments: {
    //             //           "title": "NEKSOFT",
    //             //         },
    //             //       ),
    //             //       child: Image.asset(
    //             //         "assets/images/nk.jpg",
    //             //         height: 100,
    //             //         width: 100,
    //             //         fit: BoxFit.fill,
    //             //       ),
    //             //     ),
    //             //     SizedBox(
    //             //       height: 05,
    //             //     ),
    //             //     InkWell(
    //             //       onTap: () => Get.toNamed(
    //             //         Paths.GroupdetailScreen,
    //             //         arguments: {
    //             //           "title": "PAK SUFYAN STARS",
    //             //         },
    //             //       ),
    //             //       child: Image.asset(
    //             //         "assets/images/sufyan.jpeg",
    //             //         height: 100,
    //             //         width: 100,
    //             //         fit: BoxFit.fill,
    //             //       ),
    //             //     ),
    //             //     SizedBox(
    //             //       height: 05,
    //             //     ),
    //             //     InkWell(
    //             //       onTap: () => Get.toNamed(
    //             //         Paths.GroupdetailScreen,
    //             //         arguments: {
    //             //           "title": "KNB KINGS",
    //             //         },
    //             //       ),
    //             //       child: Image.asset(
    //             //         "assets/images/kN.jpeg",
    //             //         height: 100,
    //             //         width: 100,
    //             //         fit: BoxFit.fill,
    //             //       ),
    //             //     ),
    //             //   ],
    //             // ),
    //             // Column(
    //             //   mainAxisAlignment: MainAxisAlignment.start,
    //             //   crossAxisAlignment: CrossAxisAlignment.start,
    //             //   children: [
    //             //     InkWell(
    //             //       onTap: () => Get.toNamed(
    //             //         Paths.GroupdetailScreen,
    //             //         arguments: {
    //             //           "title": "BADAMI CC",
    //             //         },
    //             //       ),
    //             //       child: Image.asset(
    //             //         "assets/images/badamy.jpeg",
    //             //         height: 100,
    //             //         width: 100,
    //             //         fit: BoxFit.fill,
    //             //       ),
    //             //     ),
    //             //     SizedBox(
    //             //       height: 05,
    //             //     ),
    //             //     InkWell(
    //             //       onTap: () => Get.toNamed(
    //             //         Paths.GroupdetailScreen,
    //             //         arguments: {
    //             //           "title": "FRONT CC",
    //             //         },
    //             //       ),
    //             //       child: Image.asset(
    //             //         "assets/images/front.jpeg",
    //             //         height: 100,
    //             //         width: 100,
    //             //         fit: BoxFit.fill,
    //             //       ),
    //             //     ),
    //             //     SizedBox(
    //             //       height: 05,
    //             //     ),
    //             //     InkWell(
    //             //       onTap: () => Get.toNamed(
    //             //         Paths.GroupdetailScreen,
    //             //         arguments: {
    //             //           "title": "JAMIL SURWA SULTANS CC",
    //             //         },
    //             //       ),
    //             //       child: Image.asset(
    //             //         "assets/images/jamil.jpeg",
    //             //         height: 100,
    //             //         width: 100,
    //             //         fit: BoxFit.fill,
    //             //       ),
    //             //     ),
    //             //     SizedBox(
    //             //       height: 05,
    //             //     ),
    //             //     InkWell(
    //             //       onTap: () => Get.toNamed(
    //             //         Paths.GroupdetailScreen,
    //             //         arguments: {
    //             //           "title": "CHANDIO ROYAL",
    //             //         },
    //             //       ),
    //             //       child: Image.asset(
    //             //         "assets/images/royal.jpeg",
    //             //         height: 100,
    //             //         width: 100,
    //             //         fit: BoxFit.fill,
    //             //       ),
    //             //     ),
    //             //     SizedBox(
    //             //       height: 05,
    //             //     ),
    //             //     InkWell(
    //             //       onTap: () => Get.toNamed(
    //             //         Paths.GroupdetailScreen,
    //             //         arguments: {
    //             //           "title": "BANGILANE SUPERSTAR",
    //             //         },
    //             //       ),
    //             //       child: Image.asset(
    //             //         "assets/images/BBSS.jpeg",
    //             //         height: 100,
    //             //         width: 100,
    //             //         fit: BoxFit.fill,
    //             //       ),
    //             //     ),
    //             //   ],
    //             // ),
    //           ],
    //         ),
    //         Text(
    //           'Note:\n All the teams from the group will play 4 matches.',
    //           textAlign: TextAlign.center,
    //           style: TextStyle(
    //               fontSize: 15,
    //               fontWeight: FontWeight.w700,
    //               fontStyle: FontStyle.italic),
    //         )
    //       ],
    //     ),
    //   ),
    // ),
  }
}
