

import 'package:get/state_manager.dart';

class Scorecardcontroller extends GetxController{
  
   bool isSelected = true;
    String? balls;
    String dropdownvalue = 'Wicket'; 
    var items = [    
    'Wicket',
    'Caught',
    'Bowled',
    'Stumped',
    'RunOut(Striker)',
     'RunOut(Non-Striker)',
      'LBW',
       'Hit-Wicket',
        'Obstructing Field',
         'Handling the ball',
  ]; 
  List ball=[
    {
      "num":"0",
      "isActive":true,
    },
    {
      "num":"1",
      "isActive":false,
    },{
      "num":"2",
      "isActive":false,
    },{
      "num":"3",
      "isActive":false,
    },{
      "num":"4",
      "isActive":false,
    },{
      "num":"5",
      "isActive":false,
    },{
      "num":"6",
      "isActive":false,
    },
  ];
  radioClick(dynamic item){
    item["isActive"]=!item["isActive"];
    update();
  }
  List over=[

      {"text":"wide",
       "isActive":false,},
      {"text":"No ball",
       "isActive":false,},
      {"text":"Byes",
       "isActive":false,},
      {"text":"Leg Byes",
       "isActive":false,},
    
  ];
  checkbox(dynamic item){
    item["isActive"]=!item["isActive"];
    update();
  }
}