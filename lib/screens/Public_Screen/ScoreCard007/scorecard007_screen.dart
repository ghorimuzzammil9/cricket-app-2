import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/Public_Screen/ScoreCard007/scorecard007_controller.dart';


class ScoreCardScreen extends GetView<Scorecardcontroller> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<Scorecardcontroller>(
      builder: (controller) {
        return Scaffold(
            backgroundColor: Color.fromRGBO(234, 235, 236, 1),
            appBar: AppBar(
              backgroundColor: Color.fromRGBO(13, 48, 47, 1),
              automaticallyImplyLeading: true,
              actions: <Widget>[
                Row(
                  children: [
                    Text(
                      "UNDO",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Color.fromARGB(255, 255, 255, 255),
                      ),
                    ),
                  ],
                ),
                IconButton(
                  icon: Icon(
                    Icons.more_vert_sharp,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    // do something
                  },
                )
              ],
              title: Text(
                "Cricket ScoreCard",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  color: Color.fromARGB(255, 255, 255, 255),
                ),
              ),
              scrolledUnderElevation: 0,
            ),
            body: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    height: 65,
                    margin: EdgeInsets.only(bottom: 0, top: 10),
                    padding: EdgeInsets.only(
                        bottom: 10, top: 0, left: 10, right: 10),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Neksoft",
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                              ),
                              SizedBox(
                                width: Get.width * .50,
                                child: Text(
                                  "CCR",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromARGB(255, 0, 0, 0),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                child: Text(
                                  "0-0(0.0/20)",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Color.fromARGB(255, 0, 0, 0),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: Get.width * .50,
                                child: Text(
                                  "0-0(0.0/20)",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Color.fromARGB(255, 0, 0, 0),
                                  ),
                                ),
                              ),
                            ]),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    height: 33,
                    margin: EdgeInsets.only(bottom: 0, top: 5),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "P'SHIP 0(0)",
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                              ),
                              
                              Text(
                                "Player1, 0(0)",
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                              ),
                              Container(
                                width: Get.width * .20,
                                child: Text(
                                  "Player1, 0(0)",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: Color.fromARGB(255, 0, 0, 0),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    height: 50,
                    margin: EdgeInsets.only(bottom: 0, top: 5),
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.over.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: Row(
                              children: [
                                GestureDetector(
                                  onTap: () => controller
                                      .checkbox(controller.over[index]),
                                  child: controller.over[index]["isActive"]
                                      ? Icon(Icons.check_box_outline_blank)
                                      : Icon(
                                          Icons.check_box_outlined,
                                        ),
                                ),
                                Container(
                                    margin: EdgeInsets.only(left: 5),
                                    child: Text(
                                      controller.over[index]["text"].toString(),
                                    ))
                              ],
                            ),
                          );
                        }),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    height: 50,
                    margin: EdgeInsets.only(bottom: 0, top: 5),
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.ball.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            margin: EdgeInsets.only(left: 7, right: 7),
                            child: Row(
                              children: [
                                GestureDetector(
                                  onTap: () => controller
                                      .radioClick(controller.ball[index]),
                                  child: controller.ball[index]["isActive"]
                                      ? Icon(Icons.radio_button_checked)
                                      : Icon(Icons.radio_button_off),
                                ),
                                Container(
                                    margin: EdgeInsets.only(left: 5),
                                    child: Text(
                                      controller.ball[index]["num"].toString(),
                                    ))
                              ],
                            ),
                          );
                        }),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    height: 33,
                    margin: EdgeInsets.only(bottom: 0, top: 5),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "THIS OVER :",
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    margin: EdgeInsets.only(bottom: 0, top: 5),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.blueAccent),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, right: 15),
                                  child: TextButton(
                                    style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                    ),
                                    onPressed: () {
                                       showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
    actions: [
      TextButton(onPressed: () {
        
      },  child: Text('Striker',),
      ),
      TextButton(onPressed: () {
        
      },  child: Text('Non-Striker',),
      )
    ],
  );
    },
  );
                                    },
                                    child: Text(
                                      'RETIRE',
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        color: Color.fromARGB(255, 0, 0, 0),
                                      ),
                                    ),
                                  ),
                                ),
                              ),

                              // DropdownButton(

                              // // Initial Value
                              // value:controller. dropdownvalue,

                              // // Down Arrow Icon
                              // // icon: const Icon(Icons.keyboard_arrow_down),

                              // // Array list of items

                              // items:controller. items.map((String items) {
                              //   return DropdownMenuItem(

                              //     value: items,
                              //     child: Text(items),

                              //   );
                              // }).toList(), onChanged: (Object? value) {  },

                              //           ),

                              Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                child: DropdownButton<String>(
                                  value: controller.dropdownvalue,
                                  icon: const Icon(Icons.arrow_downward),
                                  elevation: 0,
                                  style: const TextStyle(
                                      color: Color.fromARGB(255, 0, 0, 0)),
                                  isExpanded: false,
                                  isDense: false,
                                  onChanged: (String? value) {},
                                  items: controller.items
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),

                                ),
                              ),

                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.blueAccent),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, right: 15),
                                  child: TextButton(
                                    style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                    ),
                                    onPressed: () {
                                    
                                    },
                                    child: Text(
                                      'OK',
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        color: Color.fromARGB(255, 0, 0, 0),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                    margin: EdgeInsets.only(bottom: 0, top: 5),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.blueAccent),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, right: 15),
                                  child: TextButton(
                                    style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                    ),
                                    onPressed: (
                          
                                    ) {
                                    

  // set up the button

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
    content: Text("Are you sure want to end innings?"),
    actions: [
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          TextButton(onPressed: () {
            
          },  child: Text('NO',),
          ),
          TextButton(onPressed: () {
            
          },  child: Text('YES',),
          ),
        ],
      )
    ],
  );
    },
  );
  
                                    },
                                    child: Text(
                                      'END INNINGS',
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        color: Color.fromARGB(255, 0, 0, 0),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.blueAccent),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, right: 15),
                                  child: TextButton(
                                    style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                    ),
                                    onPressed: () {},
                                    child: Text(
                                      'SWITCH BAT',
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        color: Color.fromARGB(255, 0, 0, 0),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(   
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.blueAccent),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, right: 15),
                                  child: TextButton(
                                    style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.black),
                                    ),
                                    onPressed: () {},
                                    child: Text(
                                      'EDIT BALL',
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.red,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                         
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                      ),
                      margin: EdgeInsets.only(bottom: 0, top: 5),
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                    top: 20, left: 20, right: 20),
                                child: Image.asset(
                                  "assets/images/bating1.jpg",
                                  height: 60,
                                  width: 60,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(height: Get.height*.045,
                                  padding: EdgeInsets.only(left: 10,right: 10),
                                   
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(
                                          color: Color.fromARGB(255, 0, 0, 0)),
                                    ),
                                    child: TextButton(
                                      style: ButtonStyle(
                                        foregroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.black),
                                      ),
                                      onPressed: () {},
                                      child: Text(
                                        'player',
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Color.fromARGB(255, 0, 0, 0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 30,right: 10,top: 10),
                                    child: Text("0(0)",
                                    ),
                                  )
                                ],
                              ),
                              Column(children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: 20, left: 20, right: 20),
                                      child: Image.asset(
                                        "assets/images/bow.jpg",
                                        height: 60,
                                        width: 60,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(height: Get.height*.045,
                                         padding: EdgeInsets.only(left: 10,right: 10),
                                         
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            border: Border.all(
                                                color: Color.fromARGB(255, 0, 0, 0)),
                                          ),
                                          child: TextButton(
                                            style: ButtonStyle(
                                              foregroundColor:
                                                  MaterialStateProperty.all<Color>(
                                                      Colors.black),
                                            ),
                                            onPressed: () {},
                                            child: Text(
                                              'player3',
                                              style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromARGB(255, 0, 0, 0),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 30,right: 10,top: 10),
                                          child: Text("0(0)",
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ])
                              
                ],
                ),
                ),
                Column(children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                    top: 20, left: 20, right: 20),
                                child: Image.asset(
                                  "assets/images/bating1.jpg",
                                  height: 60,
                                  width: 60,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(height: Get.height*.045,
                                  padding: EdgeInsets.only(left: 10,right: 10),
                                   
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(
                                          color: Color.fromARGB(255, 0, 0, 0)),
                                    ),
                                    child: TextButton(
                                      style: ButtonStyle(
                                        foregroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.black),
                                      ),
                                      onPressed: () {},
                                      child: Text(
                                        'player2',
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Color.fromARGB(255, 0, 0, 0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 30,right: 10,top: 10),
                                    child: Text("0(0)",
                                    ),
                                  )
                                ],
                              ),
                              Column(children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: 20, left: 20, right: 20),
                                      child: Image.asset(
                                        "assets/images/bow.jpg",
                                        height: 60,
                                        width: 60,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(height: Get.height*.045,
                                         padding: EdgeInsets.only(left: 10,right: 10),
                                         
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            border: Border.all(
                                                color: Color.fromARGB(255, 0, 0, 0)),
                                          ),
                                          child: TextButton(
                                            style: ButtonStyle(
                                              foregroundColor:
                                                  MaterialStateProperty.all<Color>(
                                                      Colors.black),
                                            ),
                                            onPressed: () {},
                                            child: Text(
                                              'player4',
                                              style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                color: Color.fromARGB(255, 0, 0, 0),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 30,right: 10,top: 10),
                                          child: Text("0(0)",
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                              ),
                              
                ],
                ),
                ),
                ],
                ),
               
                ],
                ),
                ),
                  
                 
                ]
                
                ,
                ),
                )
                ,
              );

      },
    );
  }
}
