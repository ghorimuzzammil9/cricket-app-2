import 'package:get/get.dart';

import 'Scorebook_controller.dart';


class ScoreBookBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(ScoreBookController());
  }

}