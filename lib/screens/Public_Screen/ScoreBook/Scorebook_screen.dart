import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Utills/Paths.dart';
import 'Scorebook_controller.dart';

class ScoreBookScreen extends GetView<ScoreBookController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ScoreBookController>(
      init: ScoreBookController(),
      builder: (controller) {
        return Scaffold(
          backgroundColor: Color.fromRGBO(234, 235, 236, 1),
          appBar: AppBar(
            backgroundColor:  Color.fromRGBO(13, 48, 47, 1),
            elevation: 0,
            title: Text(
              'Cricket ScoreCard',
                          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Color.fromARGB(255, 255, 255, 255),
            ),
            ),
            actions: [
              IconButton(
                onPressed: (
                  
                ) {
                   
            Get.toNamed(Paths.MyProfileScreen);
                },
                icon: Icon(Icons.person_rounded),
              )
            ],
          ),
          body: Container(
            margin: EdgeInsets.all(7),
            color: Color.fromRGBO(234, 235, 236, 1),
            child: Column(
              children: [
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: controller.list.length,
                    itemBuilder: (BuildContext contaxt, int index) {
                      return GestureDetector(
                        onTap: () {
                          Get.toNamed(controller.list[index]["path"].toString());
                        },
                        child: Container(
                          height: Get.height*0.13,
                          padding: EdgeInsets.all(8),
                          margin: EdgeInsets.only(top: 15, left: 10, right: 10),
                         
                          decoration: BoxDecoration( color: Color.fromRGBO(13, 48, 47, 1),
                            borderRadius: BorderRadius.circular(10)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Image.asset(
                                controller.list[index]["img"].toString(),                              
                                height: 50,
                                width: 50,
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 40),
                                child: Text(
                                  controller.list[index]["text"].toString(),
                                
                                  style: TextStyle(
                                      fontSize: 25,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
          
                //    Container(padding: EdgeInsets.all(8),
                //   margin: EdgeInsets.only(top: 10,
                //   left: 10, right: 10),
                //   color: Color.fromARGB(255,4,56,5),
                //     child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: [
                //  Image.asset("assets/images/door1.png",
                //  height: 50, width: 50,),
                //  Container(margin: EdgeInsets.only(right: 40),
                //    child: Text('OPEN MATCH', style: TextStyle(
                //     fontSize: 30, color: Colors.white,
                //     fontWeight: FontWeight.w400
                //    ),),
                //  )
                //     ],),
                //   ),
                //  Container(padding: EdgeInsets.all(8),
                //   margin: EdgeInsets.only(top: 10,
                //   left: 10, right: 10),
                //   color: Color.fromARGB(255,4,56,5),
                //     child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: [
                //  Image.asset("assets/images/stats1.png",
                //  height: 50, width: 50,),
                //  Container(margin: EdgeInsets.only(right: 40),
                //    child: Text('STATS', style: TextStyle(
                //     fontSize: 30, color: Colors.white,
                //     fontWeight: FontWeight.w400
                //    ),),
                //  )
                //     ],),
                //   ),
                //  Container(padding: EdgeInsets.all(8),
                //   margin: EdgeInsets.only(top: 10,
                //   left: 10, right: 10),
                //   color: Color.fromARGB(255,4,56,5),
                //     child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: [
                //  Image.asset("assets/images/setting1.jpg",
                //  height: 50, width: 50,),
                //  Container(margin: EdgeInsets.only(right: 40),
                //    child: Text('SETTINGS', style: TextStyle(
                //     fontSize: 30, color: Colors.white,
                //     fontWeight: FontWeight.w400
                //    ),),
                //  )
                //     ],),
                //   ),
                //  Container(padding: EdgeInsets.all(8),
                //   margin: EdgeInsets.only(top: 10,
                //   left: 10, right: 10),
                //   color: Color.fromARGB(255,4,56,5),
                //     child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: [
                //  Image.asset("assets/images/madel1.jpg",
                //  height: 50, width: 50,),
                //  Container(margin: EdgeInsets.only(right: 40),
                //    child: Text('RANKINGS', style: TextStyle(
                //     fontSize: 30, color: Colors.white,
                //     fontWeight: FontWeight.w400
                //    ),),
                //  ),
                //     ],),
                //   ),
              ],
            ),
          ),
        );
      }
    );
  }
}
