import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/Public_Screen/Points-Talble/pointstable_controller.dart';

class PointstableScreen extends GetView<PointstableController> {
  @override
  Widget build(BuildContext context) {
    //    return DefaultTabController(
    //     length: 3,
    //     child: GetBuilder<PointstableController>(builder: (controller) {
    //
    // return Scaffold(backgroundColor: Colors.white,
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(13, 48, 47, 1),
        title: Text(
          'Cricket ScoreCard',
          style: TextStyle(fontSize: 25, color: Colors.white),
        ),
      ),
            body: SafeArea(
        child: DefaultTabController(
          length: 3,
          child: Column(
            children: <Widget>[
              TabBar(
              
                unselectedLabelStyle: TextStyle(color: Colors.white),
                labelStyle:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                tabs: [
                  Tab(
                  
                    child: Container(
                      height: 40, width: Get.width*0.2,
                      child: Center(child: Text('BAT',
                      style: TextStyle(color: Colors.white),)),
                    ),
                  
                  ),
                  Tab(
                     child: Container(
                      height: 40, width: Get.width*0.2,
                      child: Center(child: Text('BALL', style: TextStyle(color: Colors.white),)),
                    ),
                  
                    
                  ),
                  Tab(
                     child: Container(
                      height: 40, width: Get.width*0.2,
                      child: Center(child: Text('TOTAL',
                       style: TextStyle(color: Colors.white),)),
                    ),
                  
                  ),
                  // Tab(icon: Icon(Icons.directions_car)),
                  // Tab(icon: Icon(Icons.directions_transit)),
                  // Tab(icon: Icon(Icons.directions_bike)),
                ],
              ),
              Expanded(
                child: TabBarView(
                  children: <Widget>[
                     Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 15, left: 5, right: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'PLAYERS',
                          style: TextStyle(fontSize: 15),
                        ),
                        Icon(
                          Icons.cloud,
                          size: 20,
                          color: Colors.grey,
                        ),
                        Text(
                          'POINTS',
                          style: TextStyle(fontSize: 15),
                        ),
                        Icon(Icons.cloud, size: 20, color: Colors.grey),
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 15, bottom: 10),
                      child: Divider(
                        thickness: 1,
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/photo123.png",
                              width: Get.width * 0.15,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 25),
                            ),
                            Text(
                              'player1',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.purple),
                            )
                          ],
                        ),
                        Text('0.0'),
                      ],
                    ),
                  ),
                  
                    Container(
                      margin: EdgeInsets.only(top: 15, bottom: 10),
                      child: Divider(
                        thickness: 2,
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/photo123.png",
                              width: Get.width * 0.15,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 25),
                            ),
                            Text(
                              'player2',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.purple),
                            )
                          ],
                        ),
                        Text('0.0'),
                      ],
                    ),
                  ),
                ]
                     ),
             
                       Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 15, left: 5, right: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'PLAYERS',
                          style: TextStyle(fontSize: 15),
                        ),
                        Icon(
                          Icons.cloud,
                          size: 20,
                          color: Colors.grey,
                        ),
                        Text(
                          'POINTS',
                          style: TextStyle(fontSize: 15),
                        ),
                        Icon(Icons.cloud, size: 20, color: Colors.grey),
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 15, bottom: 10),
                      child: Divider(
                        thickness: 1,
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/photo321.jpg",
                              width: Get.width * 0.15,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 25),
                            ),
                            Text(
                              'bowler1',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.purple),
                            )
                          ],
                        ),
                        Text('0/0'),
                      ],
                    ),
                  ),
                  
                    Container(
                      margin: EdgeInsets.only(top: 15, bottom: 10),
                      child: Divider(
                        thickness: 2,
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/photo321.jpg",
                              width: Get.width * 0.15,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 25),
                            ),
                            Text(
                              'bowler2',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.purple),
                            )
                          ],
                        ),
                        Text('0/0'),
                      ],
                    ),
                  ),
              
                ]
                     ),
                     Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 15, left: 5, right: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'PLAYERS',
                          style: TextStyle(fontSize: 15),
                        ),
                        Icon(
                          Icons.cloud,
                          size: 20,
                          color: Colors.grey,
                        ),
                        Text(
                          'POINTS',
                          style: TextStyle(fontSize: 15),
                        ),
                        Icon(Icons.cloud, size: 20, color: Colors.grey),
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 15, bottom: 10),
                      child: Divider(
                        thickness: 1,
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/photo123.png",
                              width: Get.width * 0.15,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 25),
                            ),
                            Text(
                              'player1',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.purple),
                            )
                          ],
                        ),
                        Text('0.0'),
                      ],
                    ),
                  ),
                  
                    Container(
                      margin: EdgeInsets.only(top: 15, bottom: 10),
                      child: Divider(
                        thickness: 2,
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/photo123.png",
                              width: Get.width * 0.15,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 25),
                            ),
                            Text(
                              'player2',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.purple),
                            )
                          ],
                        ),
                        Text('0.0'),
                      ],
                    ),
                  ),
                ]
                     ),
             
                  //  Container(
                  //     margin: EdgeInsets.only(top: 15, bottom: 10),
                  //     child: Divider(
                  //       thickness: 1,
                  //     ),
                  //     ),
                  // Container(
                  //   margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //     children: [
                  //       Row(
                  //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //         children: [
                  //           Image.asset(
                  //             "assets/images/photo123.png",
                  //             width: Get.width * 0.15,
                  //           ),
                  //           Container(
                  //             margin: EdgeInsets.only(left: 25),
                  //           ),
                  //           Text(
                  //             'player1',
                  //             style:
                  //                 TextStyle(fontSize: 15, color: Colors.purple),
                  //           )
                  //         ],
                  //       ),
                  //       Text('0.0'),
                  //     ],
                  //   ),
                  // ),
                  
                
             

             

                    Center(
                      child: Icon(Icons.directions_transit),
                    ),
                
                 Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 15, left: 5, right: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'PLAYERS',
                          style: TextStyle(fontSize: 15),
                        ),
                        Icon(
                          Icons.cloud,
                          size: 20,
                          color: Colors.grey,
                        ),
                        Text(
                          'POINTS',
                          style: TextStyle(fontSize: 15),
                        ),
                        Icon(Icons.cloud, size: 20, color: Colors.grey),
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 15, bottom: 10),
                      child: Divider(
                        thickness: 1
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/photo123.png",
                              width: Get.width * 0.15,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 25),
                            ),
                            Text(
                              'player1',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.purple),
                            )
                          ],
                        ),
                        Text('0.0'),
                      ],
                    ),
                  ),
                    Container(
                      margin: EdgeInsets.only(top: 15, bottom: 10),
                      child: Divider(
                        thickness: 2,
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/photo123.png",
                              width: Get.width * 0.15,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 25),
                            ),
                            Text(
                              'player2',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.purple),
                            )
                          ],
                        ),
                        Text('0.0'),
                      ],
                    ),
                  ),
                ],
              ),

                 
            ]),
            ),
            ]
            ),
            ),
            
                  


                    // Center(
                    //   child: Icon(Icons.directions_car),
                    // ),
                    // Center(
                    //   child: Icon(Icons.directions_transit),
                    // ),
                    // Center(
                    //   child: Icon(Icons.directions_bike),
                    
        ));}
                
            
      
  }


          
              // Column(
              //   children: [
              //     Container(
              //       margin: EdgeInsets.only(top: 15, left: 5, right: 5),
              //       child: Row(
              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //         children: [
              //           Text(
              //             'PLAYERS',
              //             style: TextStyle(fontSize: 15),
              //           ),
              //           Icon(
              //             Icons.cloud,
              //             size: 20,
              //             color: Colors.grey,
              //           ),
              //           Text(
              //             'POINTS',
              //             style: TextStyle(fontSize: 15),
              //           ),
              //           Icon(Icons.cloud, size: 20, color: Colors.grey),
              //         ],
              //       ),
              //     ),
              //     Container(
              //         margin: EdgeInsets.only(top: 15, bottom: 10),
              //         child: Divider(
              //           thickness: 1,
              //         )),
              //     Container(
              //       margin: EdgeInsets.only(top: 10, left: 20, right: 20),
              //       child: Row(
              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //         children: [
              //           Row(
              //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //             children: [
              //               Image.asset(
              //                 "assets/images/photo123.png",
              //                 width: Get.width * 0.15,
              //               ),
              //               Container(
              //                 margin: EdgeInsets.only(left: 25),
              //               ),
              //               Text(
              //                 'player1',
              //                 style:
              //                     TextStyle(fontSize: 15, color: Colors.purple),
              //               )
              //             ],
              //           ),
              //           Text('0.0'),
              //         ],
              //       ),
              //     ),
              //     Container(
              //         margin: EdgeInsets.only(top: 15, bottom: 10),
              //         child: Divider(
              //           thickness: 1,
              //         )),
              //     Container(
              //       margin: EdgeInsets.only(top: 10, left: 20, right: 20),
              //       child: Row(
              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //         children: [
              //           Row(
              //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //             children: [
              //               Image.asset(
              //                 "assets/images/photo123.png",
              //                 width: Get.width * 0.15,
              //               ),
              //               Container(
              //                 margin: EdgeInsets.only(left: 25),
              //               ),
              //               Text(
              //                 'player2',
              //                 style:
              //                     TextStyle(fontSize: 15, color: Colors.purple),
              //               )
              //             ],
              //           ),
              //           Text('0.0'),
              //         ],
              //       ),
              //     ),
              //   ],
              // ),
              // Container(
              //     margin: EdgeInsets.only(top: 15, bottom: 15),
              //     child: Divider(
              //       thickness: 2,
              //       color: Colors.red,
              //     )),
              //      Column(
              //   children: [
              //     Container(
              //       margin: EdgeInsets.only(top: 15, left: 5, right: 5),
              //       child: Row(
              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //         children: [
              //           Text(
              //             'PLAYERS',
              //             style: TextStyle(fontSize: 15),
              //           ),
              //           Icon(
              //             Icons.cloud,
              //             size: 20,
              //             color: Colors.grey,
              //           ),
              //           Text(
              //             'POINTS',
              //             style: TextStyle(fontSize: 15),
              //           ),
              //           Icon(Icons.cloud, size: 20, color: Colors.grey),
              //         ],
              //       ),
              //     ),
              //     Container(
              //         margin: EdgeInsets.only(top: 15, bottom: 10),
              //         child: Divider(
              //           thickness: 1,
              //         )),
              //     Container(
              //       margin: EdgeInsets.only(top: 10, left: 20, right: 20),
              //       child: Row(
              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //         children: [
              //           Row(
              //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //             children: [
              //               Image.asset(
              //                 "assets/images/photo123.png",
              //                 width: Get.width * 0.15,
              //               ),
              //               Container(
              //                 margin: EdgeInsets.only(left: 25),
              //               ),
              //               Text(
              //                 'player1',
              //                 style:
              //                     TextStyle(fontSize: 15, color: Colors.purple),
              //               )
              //             ],
              //           ),
              //           Text('0.0'),
              //         ],
              //       ),
              //     ),
              //     Container(
              //         margin: EdgeInsets.only(top: 15, bottom: 10),
              //         child: Divider(
              //           thickness: 1,
              //         )),
              //     Container(
              //       margin: EdgeInsets.only(top: 10, left: 20, right: 20),
              //       child: Row(
              //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //         children: [
              //           Row(
              //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //             children: [
              //               Image.asset(
              //                 "assets/images/photo123.png",
              //                 width: Get.width * 0.15,
              //               ),
              //               Container(
              //                 margin: EdgeInsets.only(left: 25),
              //               ),
              //               Text(
              //                 'player2',
              //                 style:
              //                     TextStyle(fontSize: 15, color: Colors.purple),
              //               )
              //             ],
              //           ),
              //           Text('0.0'),
              //         ],
              //       ),
              //     ),
              //   ],
              // ),
              

 
