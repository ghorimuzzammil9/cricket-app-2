import 'package:get/get.dart';
import 'package:project_name/screens/Public_Screen/Points-Talble/pointstable_controller.dart';

class PointstableBinding extends Bindings{
  @override
  void dependencies (){
    Get.put(PointstableController());
  
}}