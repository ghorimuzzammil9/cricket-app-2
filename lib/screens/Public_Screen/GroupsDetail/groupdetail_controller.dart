import 'package:get/get.dart';

class Groupdetailcontroller extends GetxController {
  String? title;
  dynamic teamToBind = {};
  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null) {
      title = Get.arguments["title"];
      if(title=="TITANS WARRIORS"){
        teamToBind = titanWarrior;
      }
        else if(title=="BABAR & SONS CONST"){
        teamToBind = bsconst;
      }
         else if(title=="NEKSOFT"){
         teamToBind = neksoft;
      }
        else if(title=="KNB KINGS"){
       teamToBind = knb;
      }
       else if(title=="PAK SUFYAN STARS"){
       teamToBind = paksuyan;
      }
      else if(title=="BADAMI CC"){
       teamToBind =  badami;
      }
       else if(title=="FRONT CC"){

       teamToBind = front;
      }
        else if(title=="JAMIL SURWA SULTANS CC"){
        teamToBind = jamil;
      }
     
       else if(title=="BANGILANE SUPERSTAR"){
        teamToBind = bangilane;
      }
       else if(title=="CHANDIO ROYAL"){
        teamToBind = chandioRoyal;
      }
      update();
    }
  }








  dynamic titanWarrior = {
    "title": "TITANS WARRIORS",
    "team": [
      {
        "name": "KASHIF SESUDIA",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 248,
        "runs": 1218,
        "hg": "91*  (5/14)",
        "imageUrl": "assets/images/TW1.jpg"
      },
      {
        "name": "HUNAIN GULZAR",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "All Rounder",
        "ings": 240,
        "runs": 2200,
        "hg": "102  (5/21)",
        "imageUrl": "assets/images/TW2.jpg"
      },
      {
        "name": "AMAN TAJ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 42,
        "runs": 698,
        "hg": "66  (4/20)",
        "imageUrl": "assets/images/TW3.jpg"
      },
      {
        "name": "ABDURRAHMAN ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 238,
        "runs": 2117,
        "hg": "69*",
        "imageUrl": "assets/images/TW4.jpg"
      },
      {
        "name": "AZEEM DGC",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 108,
        "runs": 998,
        "hg": "55*",
        "imageUrl": "assets/images/TW14.jpg"
      },
      {
        "name": "AZZAM",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 118,
        "runs": 1198,
        "hg": "43*",
        "imageUrl": "assets/images/TW6.jpg"
      },
      {
        "name": "AMIN GHORI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 228,
        "runs": 1098,
        "hg": "57",
        "imageUrl": "assets/images/TW10.jpg"
      },
      {
        "name": "ISHAQ (E)",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 18,
        "runs": 98,
        "hg": "12",
        "imageUrl": "assets/images/TW12.jpg"
      },
      {
        "name": "DHANI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Fast Bowler",
        "ings": 38,
        "runs": 598,
        "hg": "(4/25)",
        "imageUrl": "assets/images/TW5.jpg"
      },
      {
        "name": "ABDULLAH HYD",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 58,
        "runs": 498,
        "hg": "44",
        "imageUrl": "assets/images/TW9.jpg"
      },
      {
        "name": "SHAHEEM",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Leg Break",
        "ings": 27,
        "runs": 2198,
        "hg": "(3/13)*",
        "imageUrl": "assets/images/TW11.jpg"
      },
      {
        "name": "KHALID",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 28,
        "runs": 98,
        "hg": "(3/26)",
        "imageUrl": "assets/images/TW1.jpg"
      },
      {
        "name": "BABOO",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 53,
        "runs": 398,
        "hg": "37",
        "imageUrl": "assets/images/TW8.jpg"
      },
      {
        "name": "HUZAFAH",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Fast Bowler",
        "ings": 18,
        "runs": 218,
        "hg": "30*",
        "imageUrl": "assets/images/TW7.jpg"
      },
      {
        "name": "YOUSUF GHORI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 22,
        "runs": 219,
        "hg": "47*",
        "imageUrl": "assets/images/TW13.jpg"
      },
    ]
  };

  dynamic bsconst = {
    "title": "BABAR & SONS CONST",
    "team": [
      {
        "name": "ZEESHAN GHORI",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 238,
        "runs": 2458,
        "hg": "87*",
        "imageUrl": "assets/images/BS1.jpg"
      },
      {
        "name": "HUNAIN GHORI",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "All Rounder",
        "ings": 240,
        "runs": 2080,
        "hg": "88*",
        "imageUrl": "assets/images/BS2.jpg"
      },
      {
        "name": "TALHA HUSAIN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 202,
        "runs": 1698,
        "hg": "97  (4/20)",
        "imageUrl": "assets/images/BS3.jpg"
      },
      {
        "name": "WALEED",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 198,
        "runs": 2217,
        "hg": "116*  (5/15)",
        "imageUrl": "assets/images/BS4.jpg"
      },
      {
        "name": "WASEEM DGC",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 108,
        "runs": 998,
        "hg": "55*  (3/25)",
        "imageUrl": "assets/images/BS11.jpg"
      },
      {
        "name": "DANISH ZIA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 158,
        "runs": 1198,
        "hg": "43*",
        "imageUrl": "assets/images/BS5.jpg"
      },
      {
        "name": "MAHAD",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 68,
        "runs": 598,
        "hg": "54*",
        "imageUrl": "assets/images/BS13.jpg"
      },
      {
        "name": "HUZAIFAH (E)",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 28,
        "runs": 198,
        "hg": "21",
        "imageUrl": "assets/images/BS6.jpg"
      },
      {
        "name": "ANUS ANIL",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Off Break",
        "ings": 68,
        "runs": 598,
        "hg": "(5/23)",
        "imageUrl": "assets/images/tutto.jpg"
      },
      {
        "name": "A.QADAR",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 58,
        "runs": 518,
        "hg": "44",
        "imageUrl": "assets/images/BS8.jpg"
      },
      {
        "name": "ARMAN GHORI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Leg Break",
        "ings": 57,
        "runs": 2198,
        "hg": "(6/13)*",
        "imageUrl": "assets/images/BS9.jpg"
      },
      {
        "name": "SONU VICKY ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Medium Fast",
        "ings": 28,
        "runs": 98,
        "hg": "(3/26)",
        "imageUrl": "assets/images/BS12.jpg"
      },
      {
        "name": "KHUZAIMAH",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 53,
        "runs": 398,
        "hg": "37",
        "imageUrl": "assets/images/BS7.jpg"
      },
      {
        "name": "SHOAIB BOBBY",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 118,
        "runs": 418,
        "hg": "52*",
        "imageUrl": "assets/images/BS10.jpg"
      },
      {
        "name": "IRAJ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 12,
        "runs": 219,
        "hg": "13*",
        "imageUrl": "assets/images/BS1.jpg"
      },
    ]
  };

  dynamic neksoft = {
    "title": "NEKSOFT",
    "team": [
      {
        "name": "SAIFULLAH",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 128,
        "runs": 2198,
        "hg": "137*",
        "imageUrl": "assets/images/SAIF.jpg"
      },
      {
        "name": "ZAEEM",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "Right Handed Batsman",
        "ings": 40,
        "runs": 1000,
        "hg": "46*",
        "imageUrl": "assets/images/ZAEEM.jpg"
      },
      {
        "name": "SOAHN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 102,
        "runs": 698,
        "hg": "46  (5/20)",
        "imageUrl": "assets/images/SOHAN.jpg"
      },
      {
        "name": "MUZZAMMIL",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 198,
        "runs": 2217,
        "hg": "89*  (5/15)",
        "imageUrl": "assets/images/MOLVI.jpg"
      },
      {
        "name": "BILLAY",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 108,
        "runs": 998,
        "hg": "55*  (3/25)",
        "imageUrl": "assets/images/BILLAY.jpg"
      },
      {
        "name": "AZEEM BHOJANY",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 118,
        "runs": 1198,
        "hg": "43*",
        "imageUrl": "assets/images/AZEEM.jpg"
      },
      {
        "name": "JAWWAD",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 228,
        "runs": 1198,
        "hg": "67*",
        "imageUrl": "assets/images/JAWWAD.jpg"
      },
      {
        "name": "WASMIL (E)",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 28,
        "runs": 298,
        "hg": "52",
        "imageUrl": "assets/images/WASMIL.jpg"
      },
      {
        "name": "SAFEER",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Fast Bowler",
        "ings": 168,
        "runs": 598,
        "hg": "37*",
        "imageUrl": "assets/images/SAFEER.jpg"
      },
      {
        "name": "KHUBAIB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 158,
        "runs": 1198,
        "hg": "44",
        "imageUrl": "assets/images/KHUBAIB.jpg"
      },
      {
        "name": "HAMDAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Leg Break",
        "ings": 57,
        "runs": 2198,
        "hg": "(6/13)*",
        "imageUrl": "assets/images/HAMDAN.jpg"
      },
      {
        "name": "SAAD",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Medium Fast",
        "ings": 28,
        "runs": 98,
        "hg": "(3/26)",
        "imageUrl": "assets/images/SAAD.jpg"
      },
      {
        "name": "SHAHEER",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 53,
        "runs": 398,
        "hg": "37",
        "imageUrl": "assets/images/SHAHEER.jpg"
      },
      {
        "name": "UMER HAFI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 18,
        "runs": 218,
        "hg": "30*",
        "imageUrl": "assets/images/UMERHAFI.jpg"
      },
      {
        "name": "IRAJ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 12,
        "runs": 219,
        "hg": "13*",
        "imageUrl": "assets/images/IRAJ.jpg"
      },
    ]
  };

  dynamic paksuyan = {
    "title": "PAK SUFYAN STARS",
    "team": [
      {
        "name": "ALI AYAZ",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 148,
        "runs": 1198,
        "hg": "57* (5/32)",
        "imageUrl": "assets/images/ALI AYAZ.jpg"
      },
      {
        "name": "FAYYAN",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "All Rounder",
        "ings": 240,
        "runs": 1800,
        "hg": "117* (5/12) ",
        "imageUrl": "assets/images/A1122.jpg"
      },
      {
        "name": "AWAIS",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 102,
        "runs": 698,
        "hg": "53  (3/20)",
        "imageUrl": "assets/images/A112233.jpg"
      },
      {
        "name": "SHAHZEEL",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 38,
        "runs": 217,
        "hg": "43  (2/15)",
        "imageUrl": "assets/images/SHAHZEL.jpg"
      },
      {
        "name": "HAMZA TAJ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "All Rounder",
        "ings": 108,
        "runs": 998,
        "hg": "55*",
        "imageUrl": "assets/images/mosamlo.jpg"
      },
      {
        "name": "SADIQ GHORI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 218,
        "runs": 1198,
        "hg": "77*",
        "imageUrl": "assets/images/A112.jpg"
      },
      {
        "name": "A.WASI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 88,
        "runs": 698,
        "hg": "45*",
        "imageUrl": "assets/images/WASI.jpg"
      },
      {
        "name": "ROSHAN (E)",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 28,
        "runs": 298,
        "hg": "32",
        "imageUrl": "assets/images/ROSHAN.jpg"
      },
      {
        "name": "ANUS NARANJA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Arm Off Break",
        "ings": 168,
        "runs": 598,
        "hg": "(5/28)",
        "imageUrl": "assets/images/NUSNARANJA.jpg"
      },
      {
        "name": "UMER HAJI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 158,
        "runs": 1198,
        "hg": "103",
        "imageUrl": "assets/images/A12.jpg"
      },
      {
        "name": "TALHA AMIR",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Leg Break",
        "ings": 17,
        "runs": 18,
        "hg": "(2/13)*",
        "imageUrl": "assets/images/TALHAAMIR.jpg"
      },
      {
        "name": "UMAIR",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Arm Medium Fast",
        "ings": 128,
        "runs": 98,
        "hg": "(4/26)",
        "imageUrl": "assets/images/UMAIR.jpg"
      },
      {
        "name": "HAMZA AYAZ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 53,
        "runs": 398,
        "hg": "52",
        "imageUrl": "assets/images/HA,ZA AYAZ.jpg"
      },
      {
        "name": "SHOAIB TAJ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 58,
        "runs": 418,
        "hg": "53*",
        "imageUrl": "assets/images/SHOAIBTAJ.jpg"
      },
      {
        "name": "OWAIM",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 12,
        "runs": 219,
        "hg": "13*",
        "imageUrl": "assets/images/A112233.jpg"
      },
    ]
  };

  dynamic badami = {
    "title": "BADAMI CC",
    "team": [
      {
        "name": "HASSAN",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 128,
        "runs": 2198,
        "hg": "98*",
        "imageUrl": "assets/images/HASSAN.jpg"
      },
      {
        "name": "ZOHAIB",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "All Roundr",
        "ings": 160,
        "runs": 1500,
        "hg": "65*",
        "imageUrl": "assets/images/ZOHAIB.jpg"
      },
      {
        "name": "ZEESHAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 128,
        "runs": 2198,
        "hg": "80*",
        "imageUrl": "assets/images/ZEESHA.jpg"
      },
      {
        "name": "TOUQEER",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 128,
        "runs": 2198,
        "hg": "70*",
        "imageUrl": "assets/images/TOUQEER.jpg"
      },
      {
        "name": "A.RAQEEB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 15,
        "runs": 245,
        "hg": "35*",
        "imageUrl": "assets/images/ABDUL RAQEEB.jpg"
      },
      {
        "name": "OSAF",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 25,
        "runs": 198,
        "hg": "37*",
        "imageUrl": "assets/images/OSAF.jpg"
      },
      {
        "name": "SHAHZEB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 35,
        "runs": 352,
        "hg": "37*",
        "imageUrl": "assets/images/SHAHZAIB.jpg"
      },
      {
        "name": "RABNAWAZ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 35,
        "runs": 298,
        "hg": "40*",
        "imageUrl": "assets/images/RABNAWAZ.jpg"
      },
      {
        "name": "SHUJA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 00,
        "runs": 00,
        "hg": "0*",
        "imageUrl": "assets/images/SHUJA.jpg"
      },
      {
        "name": "NOSHAB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "left Handed Batsman",
        "ings": 73,
        "runs": 598,
        "hg": "67*",
        "imageUrl": "assets/images/NOSHAB.jpg"
      },
      {
        "name": "AMAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 34,
        "runs": 224,
        "hg": "20",
        "imageUrl": "assets/images/aman.jpg"
      },
      {
        "name": "MUNEEB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 39,
        "runs": 498,
        "hg": "37",
        "imageUrl": "assets/images/MUNEEBB.jpg"
      },
      {
        "name": "SAIF",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 43,
        "runs": 398,
        "hg": "35*",
        "imageUrl": "assets/images/SAIF.jpg"
      },
      {
        "name": "USMAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 8,
        "runs": 37,
        "hg": "9",
        "imageUrl": "assets/images/USMAN.jpg"
      },
    ]
  };
  dynamic front = {
    "title": "FRONT CC",
    "team": [
      {
        "name": "UZAIR",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "All Roundr",
        "ings": 125,
        "runs": 2198,
        "hg": "85",
        "imageUrl": "assets/images/UZAIR.jpg"
      },
      {
        "name": "RAHEEL",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "All Roundr",
        "ings": 160,
        "runs": 1500,
        "hg": "65*",
        "imageUrl": "assets/images/RAHEEL.jpg"
      },
      {
        "name": "REHMAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "LEFT Handed Batsman",
        "ings": 46,
        "runs": 398,
        "hg": "32",
        "imageUrl": "assets/images/REHMAN.jpg"
      },
      {
        "name": "RAHEEB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 38,
        "runs": 298,
        "hg": "56*",
        "imageUrl": "assets/images/RAHEEB.jpg"
      },
      {
        "name": "ABDULLAH",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 35,
        "runs": 245,
        "hg": "50*",
        "imageUrl": "assets/images/ABDULLAH.jpg"
      },
      {
        "name": "TAYYAB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 72,
        "runs": 498,
        "hg": "36",
        "imageUrl": "assets/images/TAYYAB.jpg"
      },
      {
        "name": "SHEEZAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 67,
        "runs": 352,
        "hg": "37*",
        "imageUrl": "assets/images/SHEEZAN.jpg"
      },
      {
        "name": "MUJTABA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 42,
        "runs": 398,
        "hg": "40*",
        "imageUrl": "assets/images/mujji.jpg"
      },
      {
        "name": "SHOAIB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 60,
        "runs": 290,
        "hg": "25",
        "imageUrl": "assets/images/SHOAIB.jpg"
      },
      {
        "name": "ZOHAIB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 73,
        "runs": 598,
        "hg": "67*",
        "imageUrl": "assets/images/ZOHAIB ALAM.jpg"
      },
      {
        "name": "HAMZA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 14,
        "runs": 154,
        "hg": "28",
        "imageUrl": "assets/images/HAMZA.jpg"
      },
      {
        "name": "AKARMA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 39,
        "runs": 498,
        "hg": "37",
        "imageUrl": "assets/images/AKARMA.jpg"
      },
      {
        "name": "YOUSUF",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 23,
        "runs": 298,
        "hg": "35*",
        "imageUrl": "assets/images/YOUSUF.jpg"
      },
      {
        "name": "MUTEEB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 58,
        "runs": 785,
        "hg": "62",
        "imageUrl": "assets/images/MUTEEB.jpg"
      },
       {
        "name": "HUMAIL",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 58,
        "runs": 785,
        "hg": "62",
        "imageUrl": "assets/images/HUMAIL.jpg"
      },
    ]
  };
  dynamic jamil = {
    "title": "JAMIL SURWA SULTANS CC",
    "team": [
      {
        "name": "SAIM",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "All Roundr",
        "ings": 125,
        "runs": 2198,
        "hg": "85",
        "imageUrl": "assets/images/SAIM.jpg"
      },
      {
        "name": "DANI NIMMO",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "All Roundr",
        "ings": 121,
        "runs": 1552,
        "hg": "96*",
        "imageUrl": "assets/images/DANI NIMMO.jpg"
      },
      {
        "name": "ADEEL",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 46,
        "runs": 398,
        "hg": "45",
        "imageUrl": "assets/images/ADEEL.jpg"
      },
      {
        "name": "SAFFULLAH",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 48,
        "runs": 498,
        "hg": "56*",
        "imageUrl": "assets/images/SAIFFULLAH.jpg"
      },
      {
        "name": "UBAIDULLAH",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 35,
        "runs": 445,
        "hg": "50*",
        "imageUrl": "assets/images/UBAIDULLAH.jpg"
      },
      {
        "name": "HARIS",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 72,
        "runs": 698,
        "hg": "46",
        "imageUrl": "assets/images/HARIS.jpg"
      },
      {
        "name": "ALI FAREED",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 67,
        "runs": 352,
        "hg": "37*",
        "imageUrl": "assets/images/ALI FAREED.jpg"
      },
      {
        "name": "ANUS",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 42,
        "runs": 398,
        "hg": "40*",
        "imageUrl": "assets/images/ANUS.jpg"
      },
      {
        "name": "USSAL",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 10,
        "runs": 190,
        "hg": "25",
        "imageUrl": "assets/images/USSAL.jpg"
      },
      {
        "name": "BAGH",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 12,
        "runs": 198,
        "hg": "28*",
        "imageUrl": "assets/images/BAGH.jpg"
      },
      {
        "name": "SHAYAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 14,
        "runs": 154,
        "hg": "21",
        "imageUrl": "assets/images/SHAYAN.jpg"
      },
      {
        "name": "ALISHAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 39,
        "runs": 498,
        "hg": "37",
        "imageUrl": "assets/images/ALISHA.jpg"
      },
      {
        "name": "ALI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 23,
        "runs": 198,
        "hg": "19*",
        "imageUrl": "assets/images/ALI.jpg"
      },
      {
        "name": "BASIM",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 58,
        "runs": 485,
        "hg": "36",
        "imageUrl": "assets/images/BASIM.jpg"
      },
    ]
  };
  dynamic knb = {
    "title": "KNB KINGS",
    "team": [
      {
        "name": "HADEED",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "Left Hand Batsman",
        "ings": 125,
        "runs": 2198,
        "hg": "85",
        "imageUrl": "assets/images/HADEED.jpg"
      },
      {
        "name": "JUNAID",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "All Roundr",
        "ings": 121,
        "runs": 1552,
        "hg": "96*",
        "imageUrl": "assets/images/JUNAID.jpg"
      },
      {
        "name": "SONAY",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 46,
        "runs": 398,
        "hg": "45",
        "imageUrl": "assets/images/SONOO.jpg"
      },
      {
        "name": "MUBEEN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 48,
        "runs": 498,
        "hg": "56*",
        "imageUrl": "assets/images/MUBEEN G.jpg"
      },
      {
        "name": "MAIAJ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 35,
        "runs": 445,
        "hg": "50*",
        "imageUrl": "assets/images/MAIRAJ.jpg"
      },
      {
        "name": "ABDULLAH",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 72,
        "runs": 698,
        "hg": "46",
        "imageUrl": "assets/images/ABDULLAH.jpg"
      },
      {
        "name": "HASSAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 67,
        "runs": 352,
        "hg": "37*",
        "imageUrl": "assets/images/HASSAN.jpg"
      },
      {
        "name": "TALHA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 42,
        "runs": 398,
        "hg": "40*",
        "imageUrl": "assets/images/TALHA.jpg"
      },
      {
        "name": "A MUROOF",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 10,
        "runs": 190,
        "hg": "25",
        "imageUrl": "assets/images/MAROOF.jpg"
      },
      {
        "name": "A REHMAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 12,
        "runs": 198,
        "hg": "28*",
        "imageUrl": "assets/images/A.RAHMAN.jpg"
      },
      {
        "name": "ALBER",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 14,
        "runs": 154,
        "hg": "21",
        "imageUrl": "assets/images/ALBER.jpg"
      },
      {
        "name": "UZAIR",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 39,
        "runs": 498,
        "hg": "37",
        "imageUrl": "assets/images/UZAIRYT.jpg"
      },
      {
        "name": "CHOTU",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 23,
        "runs": 198,
        "hg": "39*",
        "imageUrl": "assets/images/CHOTOO.jpg"
      },
      {
        "name": "TABISH",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 58,
        "runs": 485,
        "hg": "36",
        "imageUrl": "assets/images/TABISH.jpg"
      },
    ]
  };
  dynamic bangilane = {
    "title": "BANGILANE SUPERSTAR",
    "team": [
      {
        "name": "AHSAN",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "Right Hand Batsman",
        "ings": 125,
        "runs": 2198,
        "hg": "85",
        "imageUrl": "assets/images/AHSAN FAREED.jpg"
      },
      {
        "name": "BILAWAL",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "All Roundr",
        "ings": 121,
        "runs": 1552,
        "hg": "96*",
        "imageUrl": "assets/images/BILAWAL.jpg"
      },
      {
        "name": "IBHRAHIM",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 46,
        "runs": 398,
        "hg": "45",
        "imageUrl": "assets/images/IBRAHIM.jpg"
      },
      {
        "name": "MUBASHIR",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 48,
        "runs": 498,
        "hg": "56*",
        "imageUrl": "assets/images/MUBASHIR.jpg"
      },
      {
        "name": "ARBAAZ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 135,
        "runs": 2445,
        "hg": "56*",
        "imageUrl": "assets/images/ARBAAZ.jpg"
      },
      {
        "name": "URAIB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 32,
        "runs": 498,
        "hg": "46",
        "imageUrl": "assets/images/URAIB.jpg"
      },
      {
        "name": "TEHAMA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 67,
        "runs": 352,
        "hg": "37*",
        "imageUrl": "assets/images/TIHAMA.jpg"
      },
      {
        "name": "HAFI",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 42,
        "runs": 398,
        "hg": "40*",
        "imageUrl": "assets/images/HAFI.jpg"
      },
      {
        "name": "GHATRIF",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 10,
        "runs": 190,
        "hg": "25",
        "imageUrl": "assets/images/GHITREF.jpg"
      },
      {
        "name": "AHSAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 12,
        "runs": 198,
        "hg": "28*",
        "imageUrl": "assets/images/AHSAN PAP.jpg"
      },
      {
        "name": "UTBAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 14,
        "runs": 154,
        "hg": "21",
        "imageUrl": "assets/images/UTBAN.jpg"
      },
      {
        "name": "RUSHDAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 39,
        "runs": 498,
        "hg": "37",
        "imageUrl": "assets/images/RUSHDANF.jpg"
      },
      {
        "name": "ABDULLAH",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 23,
        "runs": 198,
        "hg": "39*",
        "imageUrl": "assets/images/ABDUL.jpg"
      },
      {
        "name": "A REHMAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 58,
        "runs": 485,
        "hg": "36",
        "imageUrl": "assets/images/ABDURRAHMAN.jpg"
      },
    ]
  };
   dynamic chandioRoyal = {
    "title": "CHANDIO ROYAL",
    "team": [
      {
        "name": "MUBEEN",
        "isCaptain": true,
        "isViceCaptain": false,
        "category": "Right Hand Batsman",
        "ings": 125,
        "runs": 2198,
        "hg": "133",
        "imageUrl": "assets/images/MUBEEN.jpg"
      },
      {
        "name": "MUNEEB",
        "isCaptain": false,
        "isViceCaptain": true,
        "category": "All Roundr",
        "ings": 121,
        "runs": 1552,
        "hg": "96*",
        "imageUrl": "assets/images/MUNEEB.jpg"
      },
      {
        "name": "NEKRAJ",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 46,
        "runs": 398,
        "hg": "45",
        "imageUrl": "assets/images/NEKO.jpg"
      },
      {
        "name": "SAMAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 48,
        "runs": 498,
        "hg": "56*",
        "imageUrl": "assets/images/saman.jpg"
      },
      {
        "name": "ZEESHAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 135,
        "runs": 2445,
        "hg": "56*",
        "imageUrl": "assets/images/ZEESHAN.jpg"
      },
      {
        "name": "ANZAL",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 32,
        "runs": 498,
        "hg": "46",
        "imageUrl": "assets/images/anzal.jpg"
      },
      {
        "name": "TALHA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Left Handed Batsman",
        "ings": 67,
        "runs": 352,
        "hg": "37*",
        "imageUrl": "assets/images/taha.jpg"
      },
      {
        "name": "HAMMAD",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 42,
        "runs": 398,
        "hg": "40*",
        "imageUrl": "assets/images/HAMMAD.jpg"
      },
      {
        "name": "SHURAIF",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 10,
        "runs": 190,
        "hg": "25",
        "imageUrl": "assets/images/SHURAIF.jpg"
      },
      {
        "name": "USAMA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 12,
        "runs": 198,
        "hg": "28*",
        "imageUrl": "assets/images/USAMA.jpg"
      },
      {
        "name": "MOOSA",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 14,
        "runs": 154,
        "hg": "21",
        "imageUrl": "assets/images/MOOSA.jpg"
      },
      {
        "name": "USAID",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 39,
        "runs": 498,
        "hg": "37",
        "imageUrl": "assets/images/usaid.jpg"
      },
      {
        "name": "ITBAN",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 23,
        "runs": 198,
        "hg": "39*",
        "imageUrl": "assets/images/ITBAN.jpg"
      },
      {
        "name": "ARBAB",
        "isCaptain": false,
        "isViceCaptain": false,
        "category": "Right Handed Batsman",
        "ings": 58,
        "runs": 485,
        "hg": "36",
        "imageUrl": "assets/images/ARBAB.jpg"
      },
    ]
  };
}
