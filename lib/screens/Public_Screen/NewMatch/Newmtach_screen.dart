import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:project_name/screens/Public_Screen/NewMatch/Newmatch_cotroller.dart';
import 'package:project_name/screens/Utills/Paths.dart';

class NewMatchScreen extends GetView<NewMatchScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<newMatchcontroller>(builder: (controller) {
      return Scaffold(
        backgroundColor: Color.fromRGBO(234, 235, 236, 1),
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(13, 48, 47, 1),
          automaticallyImplyLeading: true,
          title: Text(
            "New Match",
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Color.fromARGB(255, 255, 255, 255),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                    margin: EdgeInsets.only(
                        top: Get.height * 0.02, bottom: Get.height * 0.02),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [],
                    ),
                  ),
                ),
                Text(
                  "Team1",
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color.fromARGB(255, 0, 0, 0)),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  height: 80,
                  margin: EdgeInsets.only(bottom: 25, top: 25),
                  padding:
                      EdgeInsets.only(bottom: 10, top: 10, left: 10, right: 10),
                  child: SizedBox(
                    height: 55,
                    child: TextField(
                      controller: controller.teamName,
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(),
                        focusedBorder: UnderlineInputBorder(),
                        fillColor: Colors.transparent,
                        filled: true,
                        hintText: 'Team name',
                        hintStyle: TextStyle(
                            color: Color.fromRGBO(13, 48, 47, 1),
                            fontSize: 14,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Text(
                  "Team2",
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color.fromARGB(255, 0, 0, 0)),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  height: 80,
                  margin: EdgeInsets.only(bottom: 25, top: 25),
                  padding:
                      EdgeInsets.only(bottom: 10, top: 10, left: 10, right: 10),
                  child: SizedBox(
                    height: 55,
                    child: TextField(
                      controller: controller.teamNameTwo,
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(),
                        focusedBorder: UnderlineInputBorder(),
                        fillColor: Colors.transparent,
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          // borderSide: BorderSide(color: Colors.red, width: 20)
                        ),
                        hintText: 'Team name',
                        hintStyle: TextStyle(
                            color: Color.fromRGBO(13, 48, 47, 1),
                            fontSize: 14,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Text(
                  "OVERS",
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color.fromARGB(255, 0, 0, 0)),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  height: 80,
                  margin: EdgeInsets.only(bottom: 25, top: 25),
                  padding:
                      EdgeInsets.only(bottom: 10, top: 10, left: 10, right: 10),
                  child: SizedBox(
                    height: 55,
                    child: TextField(
                      controller: controller.overs,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(),
                        focusedBorder: UnderlineInputBorder(),
                        fillColor: Colors.transparent,
                        filled: true,
                        hintText: 'Total overs',
                        hintStyle: TextStyle(
                            color: Color.fromRGBO(13, 48, 47, 1),
                            fontSize: 14,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Text(
                  "EXTRAS",
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color.fromARGB(255, 0, 0, 0)),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  height: 120,
                  margin: EdgeInsets.only(bottom: 0, top: 20),
                  padding:
                      EdgeInsets.only(bottom: 10, top: 0, left: 10, right: 10),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Runs on NO ball",
                          ),
                          Container(
                            width: 50,
                            child: TextField(
                              controller: controller.EXTRAS,
                              decoration: InputDecoration(
                                fillColor: Colors.white,

                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                  color: Color.fromRGBO(13, 48, 47, 1),
                                )),
                                filled: true, // dont forget this line
                              ),
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Runs on Wide ball"),
                          Container(
                            width: 50,
                            child: TextField(
                              decoration: InputDecoration(
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                  color: Color.fromRGBO(13, 48, 47, 1),
                                )), // dont forget this line
                              ),
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(
                    'Start Match',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                    fixedSize: Size(Get.width, 50),
                    backgroundColor: Color.fromRGBO(13, 48, 47, 1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  onPressed: () {
                      Get.toNamed(Paths.ScorecardScreen);
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: Text(
                    "Note : Team1 will bat first",
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Color.fromARGB(255, 0, 0, 0)),
                  ),
                ),
              ],
            ),
          ),
      ));
    });
  }
}
