
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'Startmatch_controller.dart';

class StartmatchScreen extends GetView<StartmatchController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(13, 48, 47, 1),
        // automaticallyImplyLeading: false,
        leading:IconButton(icon: Icon(Icons.arrow_back),onPressed: (){
Get.back();
        },),
        title: Text(
          'Cricket ScoreCard',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
        actions: [
          // SizedBox(width: 10,),
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Text(
              'UNDO',
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          //  Stack(
          //   children: [
          //     Container(
          //       child: Column(
          //         children: [
          //           Text('Update Total Overs', style: TextStyle(
          //             fontSize: 15
          //           ),),
          //             Text('Update Target', style: TextStyle(
          //             fontSize: 15
          //           ),),
                  
          //         ],
          //       ),
          //     )
          //   ],
          //  ),

           InkWell(
            onTap: () {
              controller.openmore_vert();
            },
          child: Icon(Icons.more_vert)),
          SizedBox(
            width: 10,
         ),
        ],
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(8),
                margin: EdgeInsets.only(
                  top: 5,
                  left: 5,
                  right: 5,
                ),
                height: Get.height * 0.1,
                width: Get.width,
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.black),
                  borderRadius: BorderRadius.circular(11),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 4, bottom: 4),
                          child: Text(
                            'NEKSOFT CC',
                            style: TextStyle(fontSize: 17, color: Colors.black),
                          ),
                        ),
                        Text(
                          '0-0(0.0/20)',
                          style: TextStyle(fontSize: 17, color: Colors.black),
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 4, bottom: 4),
                          child: Text(
                            'CRR',
                            style: TextStyle(fontSize: 17, color: Colors.black),
                          ),
                        ),
                        Text(
                          '0.0',
                          style: TextStyle(fontSize: 17, color: Colors.black),
                        )
                      ],
                    ),
                    Container(),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 3, right: 5, left: 5),
                height: 35,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(8)),
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "P'SHIP 0(0)  player1 0(0)",
                        style: TextStyle(fontSize: 15, color: Colors.black),
                      ),
                      Text(
                        "player2 0(0)",
                        style: TextStyle(fontSize: 15, color: Colors.black),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(top: 3, left: 5, right: 5),
                height: Get.height * 0.08,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(11),
                    border: Border.all(color: Colors.black)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          shape: RoundedRectangleBorder(
                              // borderRadius: BorderRadius.circular(10)
                              ),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("Wide")
                      ],
                    ),
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          shape: RoundedRectangleBorder(
                              // borderRadius: BorderRadius.circular(10)
                              ),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("No ball")
                      ],
                    ),
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          shape: RoundedRectangleBorder(
                              // borderRadius: BorderRadius.circular(10)
                              ),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("Byes")
                      ],
                    ),
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          shape: RoundedRectangleBorder(
                              // borderRadius: BorderRadius.circular(10)
                              ),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("Leg Byes")
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(top: 3, left: 5, right: 5),
                height: Get.height * 0.08,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(11),
                    border: Border.all(color: Colors.black)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          fillColor: MaterialStateProperty.all(Colors.black),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("1")
                      ],
                    ),
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("2")
                      ],
                    ),
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("3")
                      ],
                    ),
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("4")
                      ],
                    ),
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("5")
                      ],
                    ),
                    Row(
                      children: [
                        Checkbox(
                          // checkColor: AppConfig.themeData.primaryColor,
                          activeColor: Colors.orange,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          value: controller.isClick,
                          onChanged: (value) => controller.click(value),
                        ),
                        Text("6")
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 3, right: 5, left: 5),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(11),
                ),
                height: 30,
                width: Get.width,
                child: Center(child: Text('THIS OVER')),
              ),
              Container(
                margin: EdgeInsets.only(top: 3, left: 5, right: 5),
                height: Get.height * 0.1,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(11),
                ),
                child: Column(
                  children: [
                    Row(
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 5,
                          ),
                          width: Get.width*0.23,
                          height: Get.height*0.06,
                        
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blue),
                              borderRadius: BorderRadius.circular(11)),
                          child: Center(child: Text('RETIRE')),
                        ),
                        // Container(margin: EdgeInsets.only(
                          
                        // ),
                        // padding: EdgeInsets.all(1),
                           DropdownButton<String>(
      value: controller.dropdownValue,
      icon: const Icon(Icons.arrow_drop_down, size: 30),
      // elevation: 0,
      // style: const TextStyle(color: Colors.deepPurple),
      // underline: Container(
        // height: 2,
        // color: Colors.deepPurpleAccent,
      // ),
      onChanged: (String? value) {
        // This is called when the user selects an item.
          controller.dropdownValue = value!;
      },
      items: controller.list.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    ),
                        
                        Container(
                          margin: EdgeInsets.only(top: 5,),
                          width: Get.width*0.25,
                          height: Get.height*0.06,
                          
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blue),
                              borderRadius: BorderRadius.circular(11)),
                          child: Center(child: Text('OK')),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 3, left: 10, right: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: (){
                        controller.openDialog();
                      },
                      child: Container(
                        height: 50,
                        width: Get.width * 0.3,
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue),
                          borderRadius: BorderRadius.circular(11),
                        ),
                        child: Center(
                          child: Text(
                            'END INNINGS',
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: Get.width * 0.3,
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.blue),
                        borderRadius: BorderRadius.circular(11),
                      ),
                      child: Center(child: Text('SWITCH BAT')),
                    ),
                    Container(
                      height: 50,
                      width: Get.width * 0.3,
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.blue),
                        borderRadius: BorderRadius.circular(11),
                      ),
                      child: Center(
                          child: Text(
                        'EDIT BALL',
                        style: TextStyle(color: Colors.red),
                      )),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 3,
                  right: 5,
                  left: 5,
                ),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(11),
                  border: Border.all(width: 2, color: Colors.grey),
                ),
                child: 
                Container(
                  child: Column
                  (children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 10),
                          child: Image.asset(
                            "assets/images/bating1.jpg",
                            width: Get.width * 0.2,
                          ),
                        ),
                        Column(
                          children: [
                            InkWell(
                              onTap: () {
                                controller.openShow();
                              },
                              child: Container(
                                width: Get.width * 0.22,
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(11),
                                    border: Border.all(color: Colors.black)),
                                child: Center(child: Text('player1*')),
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Text('0(0)'))
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 10),
                          child: Image.asset("assets/images/bow.jpg",
                              width: Get.width * 0.1),
                        ),
                        Column(
                          children: [
                            InkWell(
                              onTap: () {
                               controller.openDemo(); 
                              },
                              child: Container(
                                width: Get.width * 0.22,
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(11),
                                    border: Border.all(color: Colors.black)),
                                child: Center(child: Text('player3')),
                              ),
                            ),
                            Container(
                              
                                margin: EdgeInsets.only(top: 10),
                                child: Text('0 0 0 0 0 0'))
                          ],
                        ),
                      ],
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
              
        
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 10),
                            child: Image.asset(
                              "assets/images/bating1.jpg",
                              width: Get.width * 0.2,
                            ),
                          ),
                          Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  controller.openList();
                                } ,
                child: 
                               
                              Container(
                                width: Get.width * 0.22,
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(11),
                                    border: Border.all(color: Colors.black)),
                                child: Center(child: Text('player2')),
                              ),),
                              Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Text('0(0)'))
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 10),
                            child: Image.asset("assets/images/bow.jpg",
                                width: Get.width * 0.1),
                          ),
                          Column(
                            children: [
                              Container(
                                width: Get.width * 0.22,
                                padding: EdgeInsets.all(10),
                                child: Center(child: Text('player4')),
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Text('0 0 0 0 0 0'))
                            ],
                          ),
                        ]),
                  ]),
                ),
              )
            ],
          ),
        ),

    
      ),
    );
  }
}
