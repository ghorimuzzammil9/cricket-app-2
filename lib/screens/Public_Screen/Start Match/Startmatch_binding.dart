import 'package:get/get.dart';

import 'Startmatch_controller.dart';

class StartmatchBinding extends Bindings{
 @override
  void dependencies(){
    Get.put(StartmatchController());

  } 
}