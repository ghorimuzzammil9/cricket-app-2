import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StartmatchController extends GetxController {
  int currentIndex = 0;
   List<String> list =['Wicket', 'Caught', 'Bowled', 'Stumped', 
     'runOut(Striker)', 'RunOut(Non-Striker)', 'LBW', 'Hit-Wicket',
     'Obstructing Field', 'Handling the ball',   ];

  String dropdownValue = "Wicket";

  ontapchange(int index) {
    currentIndex = index;
    update();
  }

  bool isClick = false;
  bool isClicked = true;
  click(value) {
    isClick = value;
    isClicked = !value;
    update();
  }

  void openDialog() {
    Get.dialog(
      AlertDialog(
        content: Text(
          'Are you sure want to end innings?',
          style: TextStyle(fontSize: 14),
        ),
        actions: [
          TextButton(
            child: Text(
              "NO",
              style: TextStyle(color: Colors.black, fontSize: 16),
            ),
            onPressed: () => Get.back(),
          ),
          TextButton(
            child: Text(
              "YES",
              style: TextStyle(color: Colors.black, fontSize: 16),
            ),
            onPressed: () => Get.back(),
          ),
        ],
      ),
    );
  }

  void openShow() {
    Get.dialog(AlertDialog(
        content: Text(
          'Current name',
          style: TextStyle(fontSize: 14),
        ),
        actions: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            TextButton(
              child: Center(
                child: Text(
                  "player1*",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              onPressed: () => Get.back(),
            ),
            TextButton(
              child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Text(
                  "New Name",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              onPressed: () => Get.back(),
            ),
            TextField(
              decoration: InputDecoration(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextButton(
                  child: Container(
                    margin: EdgeInsets.only(top: 15, left: 15),
                    height: 35,
                    width: Get.width * 0.25,
                    decoration: BoxDecoration(color: Colors.red),
                    child: Center(
                        child: Text(
                      'CANCEL',
                      style: TextStyle(color: Colors.white),
                    )),
                  ),
                  onPressed: () => Get.back(),
                ),
                TextButton(
                  child: Container(
                    margin: EdgeInsets.only(top: 15, right: 15),
                    height: 35,
                    width: Get.width * 0.25,
                    decoration: BoxDecoration(color: Colors.green),
                    child: Center(
                        child: Text(
                      'ok',
                      style: TextStyle(color: Colors.white),
                    )),
                  ),
                  onPressed: () => Get.back(),
                ),
              ],
            )
          ])
        ]));
  }

  void openList() {
    Get.dialog(
      AlertDialog(
        content: Text(
          'Current name',
          style: TextStyle(fontSize: 14),
        ),
        actions: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            TextButton(
              child: Center(
                child: Text(
                  "player2*",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              onPressed: () => Get.back(),
            ),
            TextButton(
              child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Text(
                  "New Name",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              onPressed: () => Get.back(),
            ),
            TextField(
              decoration: InputDecoration(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextButton(
                  child: Container(
                    margin: EdgeInsets.only(top: 15, left: 15),
                    height: 35,
                    width: Get.width * 0.25,
                    decoration: BoxDecoration(color: Colors.red),
                    child: Center(
                        child: Text(
                      'CANCEL',
                      style: TextStyle(color: Colors.white),
                    )),
                  ),
                  onPressed: () => Get.back(),
                ),
                TextButton(
                  child: Container(
                    margin: EdgeInsets.only(top: 15, right: 15),
                    height: 35,
                    width: Get.width * 0.25,
                    decoration: BoxDecoration(color: Colors.green),
                    child: Center(
                        child: Text(
                      'ok',
                      style: TextStyle(color: Colors.white),
                    )),
                  ),
                  onPressed: () => Get.back(),
                ),
              ],
            )
          ]),
        ],
      ),
    );
  }

  void openDemo() {
    Get.dialog(
      AlertDialog(
        content: Text(
          'Current Bowler',
          style: TextStyle(fontSize: 14),
        ),
        actions: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            TextButton(
              child: Center(
                child: Text(
                  "player3*",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              onPressed: () => Get.back(),
            ),
            TextButton(
              child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Text(
                  "New Bowler",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              ),
              onPressed: () => Get.back(),
            ),
            TextField(
              decoration: InputDecoration(),
            ),
            Container(
              margin: EdgeInsets.only(top: 10, left: 10),
              child: Text(
                'hint: Tap on cancel to continue speel',
                style: TextStyle(color: Colors.red),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextButton(
                  child: Container(
                    margin: EdgeInsets.only(top: 15, left: 15),
                    height: 35,
                    width: Get.width * 0.25,
                    decoration: BoxDecoration(color: Colors.red),
                    child: Center(
                        child: Text(
                      'CANCEL',
                      style: TextStyle(color: Colors.white),
                    )),
                  ),
                  onPressed: () => Get.back(),
                ),
                TextButton(
                  child: Container(
                    margin: EdgeInsets.only(top: 15, right: 15),
                    height: 35,
                    width: Get.width * 0.25,
                    decoration: BoxDecoration(color: Colors.green),
                    child: Center(
                        child: Text(
                      'ok',
                      style: TextStyle(color: Colors.white),
                    )),
                  ),
                  onPressed: () => Get.back(),
                ),
              ],
            )
          ]),
        ],
      ),
    );
  }
  void openmore_vert() {
  Get.dialog(
    AlertDialog(
      content: Text(
        'Update Total Overs',
        style: TextStyle(fontSize: 14),
      ),
      actions: [
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          TextButton(
            child: Center(
              child: Text(
                "Update Target",
                style: TextStyle(color: Colors.black, fontSize: 14),
              ),
            ),
            onPressed: () => Get.back(),
          ),

          // TextButton(
          //   child:  Container(margin: EdgeInsets.only(
          //     left: 10
          //   ),
          //     child: Text("New Name", style: TextStyle(color: Colors.black,
          //     fontSize: 16),),
          //   ),
          //   onPressed: () => Get.back(),
          // ),
          // TextField(
          //   decoration: InputDecoration(

          //   ),
          // ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //   children: [
          //     TextButton(
          //       child: Container(margin: EdgeInsets.only(
          //         top: 15, left: 15
          //       ),
          //         height: 35, width: Get.width*0.25,
          //         decoration: BoxDecoration(
          //           color: Colors.red

          //         ),
          //         child: Center(child: Text('CANCEL', style: TextStyle(color: Colors.white),)),
          //       ),
          //       onPressed: () => Get.back(),
          //     ),
          //      TextButton(
          //        child: Container(margin: EdgeInsets.only(
          //         top: 15, right: 15
          //                    ),
          //         height: 35, width: Get.width*0.25,
          //         decoration: BoxDecoration(
          //           color: Colors.green

          //         ),
          //         child: Center(child: Text('ok', style: TextStyle(color: Colors.white),)),
          //                    ),
          //                    onPressed: () => Get.back(),
          //      ),

          //   ],
        ])
      ],
    ),
  );
}
}


