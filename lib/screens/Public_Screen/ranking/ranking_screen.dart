import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/Public_Screen/ranking/ranking_controller.dart';
class RankingScreen extends GetView {
  @override
  Widget build(BuildContext context) {
    SingleChildScrollView();
    return GetBuilder<RankingController>(
      builder: (controller) {
        return DefaultTabController(
  length: 2,
  child: Scaffold(
      body: NestedScrollView(
    headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
      return <Widget>[
        new SliverAppBar(
         backgroundColor:  Color.fromRGBO(13, 48, 47, 1),
          
          title: Text('Rankings'),
          pinned: true,
          floating: true,
          
          bottom: TabBar( 
             indicatorSize: TabBarIndicatorSize.tab,
             
                        indicatorPadding: EdgeInsets.symmetric(),
             indicator: BoxDecoration(
    color: Color.fromARGB(255, 0, 180, 93)),
            isScrollable: false,
             //indicatorColor: Colors.grey,
          labelColor: Colors.white,
         // unselectedLabelColor: Colors.grey,
            tabs: [
              Tab(child: Text('BATTING')),
              Tab(child: Text('BOWLING')),
             
            ],
          ),
        ),
      ];
    },
    body: TabBarView(
      children: <Widget>[
                    ListView.separated(
                      shrinkWrap: true,
                      itemCount: controller.Batting.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Text(
                                controller.Batting[index]["text"],
                                 style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                              ),
                            ),
                            Container(
                              width: 80,
                             
                            ),
                          ],
                        );
                      }, separatorBuilder: (BuildContext context, int index) { 
                        return Divider(
                          color: Colors.black,
                        );
                       },
                    ),
                           ListView.separated(
                      shrinkWrap: true,
                      itemCount: controller.Bowling.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Text(
                                controller.Batting[index]["text"],
                                 style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                              ),
                            ),
                            
                          ],
                        );
                      }, separatorBuilder: (BuildContext context, int index) { 
                        return Divider(
                          color: Colors.black,
                        );
                       },
                    ),
                 

      ],
    ),
  )),
);
      },
    );
  }
}
