import 'package:get/get.dart';
import 'package:project_name/screens/Public_Screen/ranking/ranking_controller.dart';



class RankingBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(RankingController());
  }

}