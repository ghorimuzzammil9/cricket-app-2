import 'package:get/get.dart';

import 'liveuodate_controller.dart';

class LiveupdateBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(LiveupdateController());
  }
}
