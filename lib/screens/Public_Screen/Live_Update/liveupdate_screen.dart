import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/Public_Screen/Live_Update/liveuodate_controller.dart';

import '../../../widgets/scoreboard.dart';

class LiveupdateScreen extends GetView<LiveupdateController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LiveupdateController>(
      builder: (controller) {
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(13, 48, 47, 1),
            leading: InkWell(
              onTap: () => Get.back(),
              child: Icon(Icons.arrow_back),
            ),
            title: Row(
              children: [Text('TW vs NS'), Icon(Icons.arrow_drop_down)],
            ),
            actions: [
              SizedBox(width: Get.width * 0.1, child: Icon(Icons.share)),
              SizedBox(
                width: Get.width * 0.1,
                child: Icon(Icons.language_outlined),
              ),
              SizedBox(
                width: Get.width * 0.1,
                child: Icon(Icons.mode_night_rounded),
              ),
              SizedBox(
                width: Get.width * 0.1,
                child: Icon(Icons.push_pin_outlined),
              ),
            ],
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                    child: Text(
                      'Live',
                      style: TextStyle(fontSize: 15, color: Colors.red),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5, left: 10, right: 10),
                    child: Text(
                      'ASPL4.1st match. ASIFABAD GROUND< KHI >, OCT 20,2023',
                      style: TextStyle(fontSize: 12, color: Colors.black),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5, left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/tw.jpeg",
                              width: Get.width * 0.07,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                'TITANS WARRIURS',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w800),
                              ),
                            )
                          ],
                        ),
                        RichText(
                            text: TextSpan(
                                text: "00.0/20.ov",
                                style: TextStyle(
                                    fontSize: 15, color: Colors.black),
                                children: <TextSpan>[
                              const TextSpan(
                                text: " 00/0",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w800),
                              )
                            ]))
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5, left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/images/nk.jpg",
                              width: Get.width * 0.07,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                'NEKSOFT CC',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w800),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 10),
                    child: Text(
                      'NS won the toss & elected bowl first',
                      style: TextStyle(fontSize: 12, color: Colors.black),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5, left: 10),
                    child: Text(
                      'Current Run Rate 0.00',
                      style: TextStyle(fontSize: 12, color: Colors.black),
                    ),
                  ),
                ],
              ),
              TabBar(
                controller: controller.tabController,
                isScrollable: true,
                padding: EdgeInsets.all(8),
                physics: NeverScrollableScrollPhysics(),
                labelColor: Color.fromARGB(255, 0, 0, 0),
                indicatorColor: Colors.blue,
                indicatorWeight: 4,
                indicatorSize: TabBarIndicatorSize.label,
                unselectedLabelColor: Color.fromRGBO(138, 150, 175, 1),
                tabs: [
                  Tab(
                    text: "Live",
                  ),
                  Tab(
                    text: "Scorecard",
                  ),
                  Tab(
                    text: "Commentary",
                  ),
                  Tab(
                    text: "Report",
                  ),
                  Tab(
                    text: "Live Status",
                  ),
                  Tab(
                    text: "Overs",
                  ),
                  Tab(
                    text: "Playing X1",
                  ),
                  Tab(
                    text: "News",
                  ),
                  Tab(
                    text: "Photos",
                  ),
                  Tab(
                    text: "Scorecard",
                  ),
                ],
              ),
              Expanded(
                child: TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: controller.tabController,
                  children: [
                    // LiveBoardWidget(),
                    Column(
                      children: [
                        TabBar(
                            controller: controller.tabControllerTwo,
                            isScrollable: true,
                            // padding: EdgeInsets.all(8),
                            physics: NeverScrollableScrollPhysics(),
                            labelColor: Color.fromARGB(255, 0, 0, 0),
                            indicatorColor: Colors.blue,
                            indicatorWeight: 3,
                            indicatorSize: TabBarIndicatorSize.label,
                            unselectedLabelColor:
                                Color.fromRGBO(138, 150, 175, 1),
                            tabs: [
                              Tab(
                                text: "TITANS WARRIURS",
                              ),
                              Tab(
                                text: "NEKSOFT",
                              )
                            ]),
                        ScoreBoardWidget()
                      ],
                    ),
                    Container(),
                    Container(),
                    Container(),
                    Container(),
                    Container(),
                    Container(),
                    Container(),
                    Container(),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
