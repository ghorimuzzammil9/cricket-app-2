import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'dashboardcricket_controller.dart';

class DashboardcricketScreen extends GetView<DashboardcricketController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardcricketController>(builder: (controller) {
      return Scaffold(
        body: controller.pages[controller.currentIndex],
        backgroundColor: Colors.white,
        bottomNavigationBar: BottomNavigationBar(
          selectedFontSize: 8,
          selectedItemColor: Colors.black,
          unselectedItemColor: Colors.grey,
          showSelectedLabels: true,
          selectedLabelStyle: TextStyle(height: 3),
          type: BottomNavigationBarType.fixed,
          selectedIconTheme: IconThemeData(color: Colors.black),
          unselectedIconTheme: IconThemeData(color: Colors.blueGrey),
          iconSize: 10,
          currentIndex: controller.currentIndex,
          onTap: (int index) {
            controller.ontapchange(index);
          },
          items: [
            BottomNavigationBarItem(
              label: "Home",
              icon: Icon(
                Icons.home,
                size: 30,
              ),
              // icon: Image.asset("assets/images/naviga1.png",

              // color: Colors.grey,
              // width: 30,height: 20,),
            ),
            BottomNavigationBarItem(
                label: "Score Card",
                icon: Icon(
                  Icons.document_scanner,
                  size: 30,
                )),
            // BottomNavigationBarItem(
            //   label: "MMS",
            //     icon: Container(
            //         height: 60,
            //         width: 60,
            //         decoration: BoxDecoration(
            //             color: Color.fromRGBO(242, 149, 31, 1),

            //             borderRadius: BorderRadius.circular(50)),
            //         //  color: Colors.yellow,

            //         child: Icon(Icons.add, size: 30,))),
            BottomNavigationBarItem(
              label: "points",
              icon: Icon(
                Icons.point_of_sale,
                size: 30,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.memory,
                size: 30,
              ),
              label: "Moment",
            ),
          ],
        ),
      );
    });
  }
}
