import 'package:get/get.dart';
import 'package:project_name/screens/Private_Screen/setting/setting_controller.dart';

import '../Points-Talble/pointstable_controller.dart';
import '../ScoreCard007/scorecard007_controller.dart';
import '../Start Match/Startmatch_controller.dart';
import 'dashboardcricket_controller.dart';


class DashboardcricketBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(DashboardcricketController());
     Get.put(StartmatchController());
     Get.put(Scorecardcontroller());
    Get.put(PointstableController());
        Get.put(SettingController());

   
    // Get.put(DashboardController());
  }

}