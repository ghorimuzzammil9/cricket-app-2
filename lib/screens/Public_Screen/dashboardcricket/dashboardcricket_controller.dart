
import 'package:get/get.dart';
import 'package:project_name/screens/Private_Screen/setting/setting_screen.dart';
import 'package:project_name/screens/Public_Screen/ScoreCard007/scorecard007_screen.dart';

import '../Points-Talble/pointstable_screen.dart';
class DashboardcricketController extends GetxController {
  int currentIndex = 0;
  ontapchange(int index) {
    currentIndex = index;
    update();
  }

  var pages = [
  
    // Home1Screen(), 
    ScoreCardScreen(),
    PointstableScreen(),
    ScoreCardScreen(),
     SettingScreen(),
  ];
}