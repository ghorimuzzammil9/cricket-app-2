
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'Splash_controller.dart';



class SplashScreen extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body:
      Column (mainAxisAlignment: MainAxisAlignment.center,
       children: [
        //  InkWell(onTap: () {
        //    Get.toNamed(Paths.GroupScreen);
        //  },
          //  child: 
           Image.asset("assets/images/aspl.jpeg",
           height: Get.height*0.8,
           width: Get.width*1,),
        //  ),
         //  Container(margin: EdgeInsets.only(
         //   top: 5
         //  ),
         //    child: Image.asset("assets/images/aspl4.jpeg",
         //     height: Get.height*0.3,),
         //  ),

         Container(
           margin: EdgeInsets.only(top: 20),
           child: RichText(text: TextSpan(
             text: "Organization by ",
             style: TextStyle(fontSize: 12, color: Color.fromARGB(255, 255, 213, 0), 
             fontWeight: FontWeight.w500),
              children: <TextSpan> [
               const TextSpan(
                 text: "KASHIF SESUDIA",
                             style: TextStyle(fontSize: 25, color: Color.fromARGB(255, 169, 30, 30), 
             fontWeight: FontWeight.w800),
               )
              ]
           ),
        
           ),
         ),
       ],),
    );

}}