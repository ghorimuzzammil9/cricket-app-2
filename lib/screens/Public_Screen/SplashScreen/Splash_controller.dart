import 'dart:async';
import 'package:get/get.dart';
import 'package:project_name/screens/Utills/Paths.dart';

import '../../../services/storage_service.dart';


class SplashController extends GetxController{
      StorageService storageService=Get.find<StorageService>();

  void onInit() {
    super.onInit();
    Timer(Duration(seconds: 3), () {
      Get.toNamed(Paths.LoginEmailScreen);
      if (storageService.getUser() == null ||
          storageService.getUser()!.uid == null) {
        Get.offAllNamed(Paths.LoginEmailScreen);
      } else {
         if (storageService.getUser() != null &&
          storageService.getUser()!.isCompleted == true) {
         Get.offAllNamed(Paths.dashboardcricketScreen);
      }
      else{
               Get.offAllNamed(Paths.MyProfileScreen);


      }
      }
    });
  }
  
}