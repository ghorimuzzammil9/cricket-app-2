import 'package:get/get.dart';

import 'Splash_controller.dart';

class Splashbinding extends Bindings{
  @override
  void dependencies() {
    Get.put(SplashController());
  }
}