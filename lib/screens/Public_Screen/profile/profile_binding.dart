import 'package:get/get.dart';
import 'package:project_name/screens/Public_Screen/profile/profile_confroller.dart';

class MyProfileBinding extends Bindings{
  @override
  void dependencies (){
    Get.put(MyProfileController());
  
}}