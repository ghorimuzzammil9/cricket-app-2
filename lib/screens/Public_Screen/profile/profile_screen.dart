import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/Public_Screen/profile/profile_confroller.dart';
import 'package:project_name/screens/Utills/Paths.dart';

class MyProfileScreen extends GetView<MyProfileController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MyProfileController>(
      builder: (cont) {
        return Scaffold(
            backgroundColor: Color.fromRGBO(246, 246, 247, 1),
            // bottomNavigationBar: Container(
            //     margin: EdgeInsets.only(left: 30, right: 30, bottom: 30,top: 30),
            //     child:
            //      ButtonWidget(() {
            //       Get.back();
            //     }, "saveChanges".tr)
            //     ),

            appBar: AppBar(
              scrolledUnderElevation: 0,
              leadingWidth: 30,
              backgroundColor: Color.fromRGBO(13, 48, 47, 1),
              centerTitle: true,
              leading: GestureDetector(
                onTap: () {
                  // Get.back();
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  size: 20,
                  color: Color.fromARGB(255, 255, 255, 255),
                ),
              ),
              title: Text(
                "Edit Profile".tr,
                style: TextStyle(
                    fontSize: 18,
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontWeight: FontWeight.w600),
              ),
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 30,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Container(
                        margin: EdgeInsets.only(
                            top: Get.height * 0.05, bottom: Get.height * 0.04),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            CircleAvatar(
                                radius: 50,
                                backgroundImage: cont.photoURL == null
                                    ? NetworkImage(
                                        'https://templates.joomla-monster.com/joomla30/jm-news-portal/components/com_djclassifieds/assets/images/default_profile.png')
                                    : NetworkImage(cont.photoURL!)),
                            Positioned.fill(
                              bottom: 0,
                              left: 0,
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Container(
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(13, 48, 47, 1),
                                        borderRadius: BorderRadius.circular(16)
                                        ),
                                  height: 32,
                                  width: 32,
                                  child:

                                   InkWell(
                                    onTap: () {
                                      
                                    cont.settingModalBottomSheet(context);
                                  
                                    },
                                      child: Icon(
                  Icons.camera_alt,
                  size: 20,
                  color: Color.fromARGB(255, 255, 255, 255),
                ),
                                   ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Text(
                      "Full Name".tr,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10, top: 15),
                      child: SizedBox(
                        height: 55,
                        child: TextField(
                          controller: controller.FullNameController,
                          decoration: InputDecoration(
                            hintText: 'Your Name'.tr,
                            hintStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ),
                    Text(
                      "Email Address".tr,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10, top: 15),
                      child: TextField(
                        controller: controller.EnterYourEmailAddressController,
                        decoration: InputDecoration(
                          hintText: 'Enter Your Email Address',
                          hintStyle: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Text(
                      "Country".tr,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    // Container(
                    //   margin: EdgeInsets.only(bottom: 10, top: 15),
                    //   child: TextField(
                    //     decoration: InputDecoration(
                    //       hintText: 'Pakistan'.tr,
                    //       suffixIcon: Icon(
                    //         Icons.keyboard_arrow_down_sharp,
                    //         size: 24,
                    //         color: Colors.black,
                    //       ),
                    //       hintStyle: TextStyle(
                    //            color:Colors.black,
                    //           fontSize: 12,
                    //           fontWeight: FontWeight.w400),
                    //     ),
                    //   ),
                    // ),
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Obx(() {
                            return Container(
                              width: 350, // Set your desired width here
                              child: DropdownButton<String>(
                                isExpanded:
                                    true, // This allows the dropdown to take up the full width
                                value: controller.selectedItem.value,
                                items: controller.items.map((item) {
                                  return DropdownMenuItem<String>(
                                    value: item,
                                    child: Text(item),
                                  );
                                }).toList(),
                                onChanged: ((value) {
                                  controller.updateSelectedItem(value!);

                                  // Handle the selected item change
                                }),
                              ),
                            );
                          }),
                          SizedBox(
                              height:
                                  20), // Add some space (adjust the height as needed)
                          // Add any additional widgets or components you want here
                        ],
                      ),
                    ),

                    // Text(
                    //   "City".tr,
                    //   style: TextStyle(
                    //     fontSize: 13,
                    //     fontWeight: FontWeight.w500,
                    //     color: Color.fromRGBO(0, 0, 0, 1),
                    //   ),
                    // ),
                    // Container(
                    //   margin: EdgeInsets.only(top: 15, bottom: 10),
                    //   child: TextField(
                    //        controller:controller.CityController ,
                    //     decoration: InputDecoration(
                    //       suffixIcon: Icon(
                    //         Icons.keyboard_arrow_down_sharp,
                    //         size: 20,
                    //         color: Colors.black,
                    //       ),
                    //       hintText: "Karachi".tr,
                    //       hintStyle: TextStyle(
                    //             color:Colors.black,
                    //           fontSize: 12,
                    //           fontWeight: FontWeight.w400),
                    //     ),
                    //   ),
                    // ),

                    Text(
                      "Phone Number".tr,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 15, bottom: 10),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        controller: controller.NumberController,
                        decoration: InputDecoration(
                          hintText: 'Phone Number'.tr,
                          hintStyle: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Text(
                      "Category".tr,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Obx(() {
                            return Container(
                              width: 350, // Set your desired width here
                              child: DropdownButton<String>(
                                isExpanded:
                                    true, // This allows the dropdown to take up the full width
                                value: controller.category.value,
                                items: controller.categoryList.map((items) {
                                  return DropdownMenuItem<String>(
                                    value: items,
                                    child: Text(items),
                                  );
                                }).toList(),
                                onChanged: ((value) {
                                  controller.updateSelectedCategory(value!);
                                  // Handle the selected item change
                                }),
                              ),
                            );
                          }),
                          SizedBox(
                            height: 20,
                          ), // Add some space (adjust the height as needed)
                          // Add any additional widgets or components you want here
                        ],
                      ),
                    ),

                    //  Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceAround,
                    //      children: [

                    //        Container(
                    //         margin: EdgeInsets.only(top: 10, bottom: 10),
                    //                       decoration: BoxDecoration(
                    //                         borderRadius: BorderRadius.circular(10),
                    //                         border: Border.all(color: Color.fromRGBO(13, 48, 47, 1)),
                    //                       ),
                    //                       child: Padding(
                    //                         padding: const EdgeInsets.only(
                    //                             left: 15, right: 15,),
                    //                         child: TextButton(
                    //                           style: ButtonStyle(
                    //                             foregroundColor:
                    //                                 MaterialStateProperty.all<Color>(
                    //                                     Colors.black),
                    //                           ),
                    //                           onPressed: () {},
                    //                           child: Text(
                    //                             'Right Hand',
                    //                             style: TextStyle(
                    //                               fontSize: 12,
                    //                               fontWeight: FontWeight.w600,
                    //                               color: Color.fromARGB(255, 0, 0, 0),
                    //                             ),
                    //                           ),
                    //                         ),
                    //                       ),
                    //                     ),
                    //                      Container(
                    //                       decoration: BoxDecoration(
                    //                         borderRadius: BorderRadius.circular(10),
                    //                         border: Border.all(color: Color.fromRGBO(13, 48, 47, 1)),
                    //                       ),
                    //                       child: Padding(
                    //                         padding: const EdgeInsets.only(
                    //                             left: 15, right: 15),
                    //                         child: TextButton(
                    //                           style: ButtonStyle(
                    //                             foregroundColor:
                    //                                 MaterialStateProperty.all<Color>(
                    //                                     Colors.black),
                    //                           ),
                    //                           onPressed: () {},
                    //                           child: Text(
                    //                             'Left Hand',
                    //                             style: TextStyle(
                    //                               fontSize: 12,
                    //                               fontWeight: FontWeight.w600,
                    //                               color:Color.fromRGBO(13, 48, 47, 1)
                    //                             ),
                    //                           ),
                    //                         ),
                    //                       ),
                    //                     ),
                    //      ],
                    //    ),

                    //          Text(
                    //     "Bowling Style".tr,
                    //     style: TextStyle(
                    //       fontSize: 13,
                    //       fontWeight: FontWeight.w500,
                    //       color: Color.fromRGBO(0, 0, 0, 1),
                    //     ),
                    //   ),
                    //    Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceAround,
                    //      children: [

                    //        Container(
                    //         margin: EdgeInsets.only(top: 10, bottom: 10),
                    //                       decoration: BoxDecoration(
                    //                         borderRadius: BorderRadius.circular(10),
                    //                         border: Border.all(color: Color.fromRGBO(13, 48, 47, 1)),
                    //                       ),
                    //                       child: Padding(
                    //                         padding: const EdgeInsets.only(
                    //                             left: 15, right: 15,),
                    //                         child: TextButton(
                    //                           style: ButtonStyle(
                    //                             foregroundColor:
                    //                                 MaterialStateProperty.all<Color>(
                    //                                     Colors.black),
                    //                           ),
                    //                           onPressed: () {},
                    //                           child: Text(
                    //                             'Right Aram',
                    //                             style: TextStyle(
                    //                               fontSize: 12,
                    //                               fontWeight: FontWeight.w600,
                    //                               color: Color.fromARGB(255, 0, 0, 0),
                    //                             ),
                    //                           ),
                    //                         ),
                    //                       ),
                    //                     ),
                    //                      Container(

                    //                       decoration: BoxDecoration(

                    //                         borderRadius: BorderRadius.circular(10),
                    //                         border: Border.all(color: Color.fromRGBO(13, 48, 47, 1)),
                    //                       ),
                    //                       child: Padding(
                    //                         padding: const EdgeInsets.only(
                    //                             left: 15, right: 15),
                    //                         child: TextButton(
                    //                           style: ButtonStyle(
                    //                             foregroundColor:
                    //                                 MaterialStateProperty.all<Color>(
                    //                                     Colors.black),
                    //                           ),
                    //                           onPressed: () {},
                    //                           child: Text(
                    //                             'Left Aram',
                    //                             style: TextStyle(
                    //                               fontSize: 12,
                    //                               fontWeight: FontWeight.w600,
                    //                               color:Color.fromRGBO(13, 48, 47, 1)
                    //                             ),
                    //                           ),
                    //                         ),
                    //                       ),
                    //                     ),
                    //      ],
                    //    ),

                    Text(
                      "Your Teams".tr,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Obx(() {
                            return Container(
                              width: 350, // Set your desired width here
                              child: DropdownButton<String>(
                                isExpanded:
                                    true, // This allows the dropdown to take up the full width
                                value: controller.selectedItem.value,
                                items: controller.items.map((item) {
                                  return DropdownMenuItem<String>(
                                    value: item,
                                    child: Text(item),
                                  );
                                }).toList(),
                                onChanged: ((value) {
                                  controller.updateSelectedItem(value!);

                                  // Handle the selected item change
                                }),
                              ),
                            );
                          }),
                          SizedBox(
                              height:
                                  20), // Add some space (adjust the height as needed)
                          // Add any additional widgets or components you want here
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      child: Text(
                        'Done',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        fixedSize: Size(Get.width, 50),
                        backgroundColor: Color.fromRGBO(13, 48, 47, 1),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      onPressed: () async{
                     await controller.  updateprofile();
                        Get.toNamed(Paths.dashboardcricketScreen); 
                      },
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ));
      }
    );
  }
}
