import 'package:get/get.dart';

import 'login_controller.dart';



class LoginEmailBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(LoginController());
  }

}