// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../Utills/Paths.dart';
import 'login_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';


class LoginEmailScreen extends GetView<LoginController> {
  const LoginEmailScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
      init: LoginController(),
      builder: (controller) {
        return Scaffold(
          backgroundColor: Color.fromRGBO(255, 255, 255, 1),       
          // appBar: AppBar(
          //   elevation: 0,
          //   scrolledUnderElevation: 0,
           
          //   // leading: IconButton(
          //   //     onPressed: () {
          //   //       Get.back();
          //   //     },
          //   //     icon: Icon(
          //   //       Icons.arrow_back_ios,
          //   //       size: 18,
          //   //       color: Get.theme.primaryColorDark,
          //   //     )),
          // ),
          
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: SingleChildScrollView(
                child: Container(
                  child: Container(
                    margin: EdgeInsets.only(top: Get.height*0.15),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(5),
                          width: Get.width * 0.2,
                          height: Get.height * 0.1,
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    color: Color.fromRGBO(55, 55, 55, 0.15),
                                    offset: Offset(0, 10),
                                    blurRadius: 5,
                                    spreadRadius: 0)
                              ],
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white),
                          child: Center(
                            child: Image.asset(
                              "assets/images/aspl.jpeg",
                              height: Get.height * 0.2,
                              width: Get.width * 0.2,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 15),
                              child: Text(
                                "welcome".tr,
                                
                                style: TextStyle(
                                  fontSize: 23.0,
                                  fontWeight: FontWeight.w600,
                                  color:Color.fromRGBO(13, 48, 47, 1),
                                ),
                              ),
                            )
                          ],
                              ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 40),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "signInToCountine".tr,
                                style: TextStyle(
                                    color:Color.fromRGBO(13, 48, 47, 1),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14.0),
                              )
                            ],
                          ),
                        ),
                        Form(
                         
                          child: Column(
                            children: [
                              TextFormField(
                                validator: ((value) {
                                  if (value == null || value.isEmpty) {
                                    return "Enter Your PhoneNumber".tr;
                                  }
                                  return null;
                                }),
                                keyboardType:
                                    TextInputType.numberWithOptions(signed: true),
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly,
                                ],
                          
                                decoration: InputDecoration(
                                    prefixIcon: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                      
                                        // Container(
                                        //   padding: EdgeInsets.only(right: 5),
                                        //   child: Text(
                                        //     // controller.dialCode,
                                        //     style: TextStyle(
                                        //       fontWeight: FontWeight.w500,
                                        //       fontSize: 16,
                                        //     ),
                                        //   ),
                                        // )
                                      ],
                                    ),
                                    // prefixIcon: Transform.scale(
                                    //   scale: 0.5,
                                    //   child: Image.asset(
                                    //     "assets/images/telephone.png",
                                    //   ),
                                    // ),
                                    hintText: "Enter Your Email".tr,
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12.0)),
                              ),
                              
                            ],
                          ),
                        ),
                        Form(
                         
                          child: Column(
                            children: [
                              TextFormField(
                                validator: ((value) {
                                  if (value == null || value.isEmpty) {
                                    return "Enter Your PhoneNumber".tr;
                                  }
                                  return null;
                                }),
                                keyboardType:
                                    TextInputType.numberWithOptions(signed: true),
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly,
                                ],
                          
                                decoration: InputDecoration(
                                    prefixIcon: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                      
                                        // Container(
                                        //   padding: EdgeInsets.only(right: 5),
                                        //   child: Text(
                                        //     // controller.dialCode,
                                        //     style: TextStyle(
                                        //       fontWeight: FontWeight.w500,
                                        //       fontSize: 16,
                                        //     ),
                                        //   ),
                                        // )
                                      ],
                                    ),
                                    // prefixIcon: Transform.scale(
                                    //   scale: 0.5,
                                    //   child: Image.asset(
                                    //     "assets/images/telephone.png",
                                    //   ),
                                    // ),
                                    hintText: "Enter Your Password",
                                    hintStyle: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12.0)),
                              ),
                              
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 20),
                          // alignment: Alignment.cente,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Or Sign In With",
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.black),
                              ),
                              SizedBox(
                    height: 20,
                ),
               
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [

                            InkWell(
                              onTap: (){
                                controller.googleAuthProvider();
                                
                                },
                              child: Container(
                                
                                padding: EdgeInsets.all(5),
                                width: Get.width * 0.15,
                                height: Get.height * 0.07,
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color.fromRGBO(55, 55, 55, 0.15),
                                          
                                          offset: Offset(0, 10),
                                          blurRadius: 10,
                                          spreadRadius: 0)
                                    ],
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.white),
                                child: Center(
                            
                                  child: Transform.scale(
                                    scale: 0.8,
                                    child: Image.asset(
                                      "assets/images/google.png",
                                  
                                    ),
                                  ),
                                ),
                              ),
                            ), Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                padding: EdgeInsets.all(5),
                                width: Get.width * 0.15,
                              height: Get.height * 0.07,
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color.fromRGBO(55, 55, 55, 0.15),
                                          offset: Offset(0, 10),
                                          blurRadius: 10,
                                          spreadRadius: 0)
                                    ],
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.white),
                                child: Center(
                                  child: Image.asset(
                                    "assets/images/fac.png",
                                    
                                    height: Get.height * 0.2,
                                    width: Get.width * 0.2,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        //                         Padding(
                        //   padding: const EdgeInsets.only(top: 15),
                        //   child: Row(
                        //     children: [
                        //       Text(
                        //         "or login with",
                        //         style: TextStyle(
                        //             fontSize: 12,
                        //             fontWeight: FontWeight.normal,
                        //             color:
                        //                 Get.theme.primaryColorLight),
                        //       ),
                        //       SizedBox(
                        //         width: 5,
                        //       ),
                        //       GestureDetector(
                        //         onTap: () => controller.toggleLoginType(),
                        //         child: Text(
                        //           controller.isMobileNumber
                        //               ? "Email address"
                        //               : "Phone Number",
                        //           style: TextStyle(
                        //               fontSize: 12,
                        //               fontWeight: FontWeight.w600,
                        //               color:
                        //                   Get.theme.primaryColorDark),
                        //         ),
                        //       )
                        //     ],
                        //   ),
                        // ),
                
                      ],

                    ),
                  ),
                ),
              ),
            ),
          ),
          bottomNavigationBar:        Padding(
            padding:  EdgeInsets.all(15.0),
            child: ElevatedButton(
                    child: Text(
                      'LogIn',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      fixedSize: Size(Get.width, 50),
                      backgroundColor:Color.fromRGBO(13, 48, 47, 1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onPressed: () {
                     
                     
                        
                    },
                  ),
          ),
      
        );
      },
    );
  }
}
