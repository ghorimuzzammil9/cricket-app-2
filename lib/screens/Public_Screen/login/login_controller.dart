// ignore_for_file: unused_field, unused_local_variable

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:project_name/controller/controllers/user_controller.dart';
import 'package:project_name/model/user_model.dart';
import 'package:project_name/screens/Utills/Paths.dart';

import '../../../services/storage_service.dart';

class LoginController extends GetxController {
  StorageService storageService = Get.find<StorageService>();

  UserController userController = Get.find<UserController>();

  Future<dynamic> googleAuthProvider() async {
    final FirebaseAuth auth = FirebaseAuth.instance;
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    final GoogleSignIn googleSignIn = await GoogleSignIn(
      scopes: ['email'],
    );
    try {
      if (await googleSignIn.isSignedIn()) {
        googleSignIn.disconnect();
      }
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
      if (googleUser == null) return;

      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
          idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);
      final firebaseUser = await auth.signInWithCredential(credential);
      var socialSignInDTO = {
        "email": firebaseUser.user!.email,
        "platform": "google",
        "uid": firebaseUser.user!.uid,
        "token": await firebaseUser.user!.getIdToken(),
        "fullName": firebaseUser.user!.displayName == null
            ? ""
            : firebaseUser.user!.displayName == ""
                ? ""
                : firebaseUser.user!.displayName,
        "phoneNumber": firebaseUser.user!.phoneNumber == null
            ? ""
            : firebaseUser.user!.phoneNumber == ""
                ? "03032077890"
                : firebaseUser.user!.phoneNumber,
        "photoUrl": firebaseUser.user!.photoURL == null
            ? ""
            : firebaseUser.user!.photoURL == ""
                ? ""
                : firebaseUser.user!.photoURL,
        "type": "social",
        "isCompleted": false,
      };
      // Define a new collection path (e.g., 'users')
      String collectionPath = 'users/${firebaseUser.user!.email}';

      // Create the collection

      FirebaseFirestore.instance
          .collection('users')
          .where('email', isEqualTo: firebaseUser.user!.email)
          .get()
          .then((QuerySnapshot querySnapshot) async {
        if (querySnapshot.docs.isNotEmpty) {
          DocumentSnapshot documentSnapshot = querySnapshot.docs[0];
          Map<String, dynamic>? userData =
              documentSnapshot.data() as Map<String, dynamic>?;

          if (userData != null) {
            // Convert the Map to your user model
            userController.setUser(UserModel.fromJson(userData));
            storageService.setUser(UserModel.fromJson(userData));
            if(userController.user!.isCompleted==false)
            {
                Get.toNamed(Paths.MyProfileScreen,
          arguments: {"data": socialSignInDTO, "fromLogin": true});
            }else{
Get.toNamed(Paths.dashboardcricketScreen,
         );
            }
            // Now, 'user' contains the user model
            // print('User: ${user.name}, ${user.email}');
          }
        } else {
          userController.setUser(UserModel.fromJson(socialSignInDTO));
          storageService.setUser(UserModel.fromJson(socialSignInDTO));
          final DocumentReference documentReference = FirebaseFirestore.instance
              .collection('users')
              .doc(userController.user!.uid);
     
          Get.toNamed(Paths.MyProfileScreen,
          arguments: {"data": socialSignInDTO, "fromLogin": true});
               return await documentReference.set(socialSignInDTO); 
          //your
          // firestore.collection('users').add(socialSignInDTO
          //     // Add any additional data you want to store
          //     );

        }
      });

      print('Collection created successfully!');
    
      return socialSignInDTO;
    } catch (e) {
      print(e);
    }
  }
}
