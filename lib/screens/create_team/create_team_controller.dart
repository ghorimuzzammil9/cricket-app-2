// ignore_for_file: must_call_super

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:project_name/controller/controllers/user_controller.dart';
import 'package:project_name/model/mediamodel.dart';
import 'package:project_name/model/user_model.dart';

import '../../services/storage_service.dart';
import '../../services/util_service.dart';

class CreateteamController extends GetxController {
    StorageService storageService = Get.find<StorageService>();
  UserController userController = Get.find<UserController>();
  TextEditingController numberController = TextEditingController();
  TextEditingController CityController = TextEditingController();
  TextEditingController CountryController =
      TextEditingController();
  TextEditingController EnterYourEmailAddressController =
      TextEditingController();
  TextEditingController FullNameController = TextEditingController();
  TextEditingController NumberController =
      TextEditingController();
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  String? photoURL;
  String? batingType;
  String? bowlingType;
  UserModel? user1;

  var selectedItem = 'Pakistan'.obs;
  final List<String> items = [
    'Pakistan',
    'India',
    'Bangladesh',
    'Afghanistan',
    'Nepal',
    'Srilanka',
  ];

  var category = 'All-Rounders'.obs;
  final List<String> categoryList = [
    
    // 'Left-Arm Bowlers',
    // 'Right-Arm Bowlers',
    // 'Fast Bowlers',
    // 'Medium-Pace Bowling',
    // 'Off-Spin Bowlers',
    // 'Leg-Spin Bowlers',
    // 'All-Rounders',
    // 'Right Handed Batsman',
    //  'Left Handed Batsman'
  ];

  void updateSelectedItem(String item) {
    selectedItem.value = item;
  }

  void updateSelectedCategory(String item) {
    category.value = item;
  }

  @override
  void onInit() {
    // super.onInit();
    // user1 = userController.user;
    // FullNameController =
    //     TextEditingController(text: "${userController.user!.fullName}");
    // EnterYourEmailAddressController =
    //     TextEditingController(text: "${userController.user!.email}");
    // photoURL = "${userController.user!.photoUrl}";
    // update();
  }

  updateprofile() async{
    UserModel user = UserModel(
      fullName: FullNameController.text,
      photoUrl: photoURL,
      email: EnterYourEmailAddressController.text,
      phoneNumber: NumberController.text,
      category: category.value,
      isCompleted: true,
      type: 'social',
      platform: "google",
      uid: userController.user!.uid,
    );

    try {
var a = await FirebaseFirestore.instance
        .collection("users")
        .doc(userController.user!.uid)
        .get();
if (a.exists) {
  //     final DocumentReference documentReference = FirebaseFirestore.instance
  //         .collection('users')
  //         .doc(userController.user!.uid);
  // userController.setUser(user);
  //         storageService.setUser(user);
  //     return await documentReference.update(
  //       user.toJson()
  //       //your data
  //     );
  util.showToaster('Error', 'User already exist');
      
    } else {
      final DocumentReference documentReference = FirebaseFirestore.instance
          .collection('users')
          .doc(userController.user!.uid);
            userController.setUser(user);
          storageService.setUser(user);
      return await documentReference.set(
        user.toJson()
       //your data
      );
    }
      // Docum}entReference documentReference =
      //     firestore.collection('users').doc("glkXUatFRyYeT83IjGhEGdDb01h1");

      // documentReference.update(user.toJson()
      //     // Add any additional data you want to store
      //     );
        //  userController.setUser(user);
      // util.showToaster("Success", "Successfully Updated");
   
    } catch (e) {
      print(e);
    }
  }

  UtilService util = Get.find<UtilService>();
  void settingModalBottomSheet(context) async {
    MediaModel res = await util.settingModalBottomSheet(context);
    if (user1!.photoUrl == null) {
      user1!.photoUrl = "";
    }
    user1!.photoUrl = res.url;
    photoURL = res.url;
    update();
  }
}