import 'package:get/get.dart';
import 'package:project_name/screens/create_team/create_team_controller.dart';

class CreateteamBinding extends Bindings{
  @override
  void dependencies (){
    Get.put(CreateteamController());
  
}}