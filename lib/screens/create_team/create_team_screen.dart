import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_name/screens/Utills/Paths.dart';
import 'package:project_name/screens/create_team/create_team_controller.dart';

class CreateteamScreen extends GetView<CreateteamController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CreateteamController>(
      builder: (cont) {
        return Scaffold(
          backgroundColor: Color.fromRGBO(246, 246, 247, 1),
          bottomNavigationBar: Container(
            margin: EdgeInsets.only(
              left: 15,
              bottom: 15,
              right: 15,
            ),
            child: ElevatedButton(
              child: Text(
                'Create Team',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              ),
              style: ElevatedButton.styleFrom(
                fixedSize: Size(Get.width, 50),
                backgroundColor: Color.fromRGBO(13, 48, 47, 1),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              onPressed: () async {
                await controller.updateprofile();
                Get.toNamed(Paths.dashboardcricketScreen);
              },
            ),
          ),
          appBar: AppBar(
            scrolledUnderElevation: 0,
            leadingWidth: 30,
            backgroundColor: Color.fromRGBO(13, 48, 47, 1),
            centerTitle: true,
            leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(
                Icons.arrow_back_ios,
                size: 20,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
            title: Text(
              "Create Team".tr,
              style: TextStyle(
                  fontSize: 18,
                  color: Color.fromRGBO(255, 255, 255, 1),
                  fontWeight: FontWeight.w600),
            ),
          ),
          body: Container(
            padding: EdgeInsets.symmetric(
              horizontal: 30,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(
                          top: Get.height * 0.05, bottom: Get.height * 0.04),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          CircleAvatar(
                              radius: 50,
                              backgroundImage: cont.photoURL == null
                                  ? NetworkImage(
                                      'https://templates.joomla-monster.com/joomla30/jm-news-portal/components/com_djclassifieds/assets/images/default_profile.png')
                                  : NetworkImage(cont.photoURL!)),
                          Positioned.fill(
                            bottom: 0,
                            left: 0,
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(13, 48, 47, 1),
                                    borderRadius: BorderRadius.circular(16)),
                                height: 32,
                                width: 32,
                                child: InkWell(
                                  onTap: () {
                                    cont.settingModalBottomSheet(context);
                                  },
                                  child: Icon(
                                    Icons.camera_alt,
                                    size: 20,
                                    color: Color.fromARGB(255, 255, 255, 255),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Text(
                    "Team Name".tr,
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(0, 0, 0, 1),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10, top: 15),
                    child: SizedBox(
                      height: 55,
                      child: TextField(
                        controller: controller.FullNameController,
                        decoration: InputDecoration(
                          hintText: 'Team Name'.tr,
                          hintStyle: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ),
                  Text(
                    "Address".tr,
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(0, 0, 0, 1),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10, top: 15),
                    child: TextField(
                      controller: controller.EnterYourEmailAddressController,
                      decoration: InputDecoration(
                        hintText: 'Enter Your Team Address',
                        hintStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                  Text(
                    "Country".tr,
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(0, 0, 0, 1),
                    ),
                  ),
                  SizedBox(
                    height: 05,
                  ),
                  Obx(() {
                    return Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(
                            width: 1,
                            color: Colors.grey.shade300,
                          )),
                      width: 350, // Set your desired width here
                      child: DropdownButton<String>(
                        isExpanded:
                            true, // This allows the dropdown to take up the full width
                        value: controller.selectedItem.value,
                        underline: SizedBox(),
                        items: controller.items.map((item) {
                          return DropdownMenuItem<String>(
                            value: item,
                            child: Text(item),
                          );
                        }).toList(),
                        onChanged: ((value) {
                          controller.updateSelectedItem(value!);

                          // Handle the selected item change
                        }),
                      ),
                    );
                  }),
                  SizedBox(height: 20),
                  Text(
                    "Contact Number",
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(0, 0, 0, 1),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: TextField(
                      keyboardType: TextInputType.number,
                      controller: controller.NumberController,
                      decoration: InputDecoration(
                        hintText: 'Phone Number'.tr,
                        hintStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
