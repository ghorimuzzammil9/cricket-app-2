import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../model/user_model.dart';

class DeleteUsersControoler extends GetxController {
  List<UserModel> users = [];

  late StreamSubscription postStream;

  @override
  void onInit() {
    super.onInit();
    initializeFeed();
  }

  var GroupAList = [
    {
      "title": "TITANS WARRIURS",
      "image": "assets/images/tw.jpeg",
    },
    {
      "name": "BABAR & SONS CONST",
      "image": "",
    },
    {
      "name": "NEKSOFT",
      "image": "",
    },
    {
      "name": "PAK SUFYAN STARS",
      "image": "",
    },
    {
      "name": "KN BUILDERS KING",
      "image": "",
    },
  ];

  List GroupBList = [
    {
      "name": "BADAMI STARS",
      "image": "",
    },
    {
      "name": "FRONT CC",
      "image": "",
    },
    {
      "name": "JAMIL SARWA SULTAN",
      "image": "",
    },
    {
      "name": "CHANDIO ROYALS",
      "image": "",
    },
    {
      "name": "BANGI LINE SUPER STAR",
      "image": "",
    },
  ];

  initializeFeed() {
    try {
      postStream = FirebaseFirestore.instance
          .collection('users')
          .snapshots()
          .listen((event) {
        for (var doc in event.docChanges) {
          var querySnapshot = doc.doc.data();
          var ind = users.indexWhere(
              (element) => element.email == querySnapshot!['email']);
          if (ind == -1) {
            users.add(UserModel.fromJson(querySnapshot!));
          }
        }
        update();

        update();
      });
    } catch (e) {
      print(e);
    }
  }

  deleteUser(String uid) {
    FirebaseFirestore.instance.collection('users').doc(uid).delete().then((_) {
      print("User deleted successfully");
    }).catchError((error) {
      print("Error deleting user: $error");
    });
  }

  popUp(String uid, BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10)
          ),
            content: Container(
              width: Get.width,
              height: 150,
              color: Colors.white,
              child: Column(
                children: [
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    "Are you sure you want to delete your account?",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            Get.back();
                          },
                          child: Container(
                            padding: EdgeInsets.all(8),decoration:BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                            color: Color.fromARGB(255, 223, 50, 50),

                            ),
                            child: Text(
                              'NO',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white

                                // color: Color.fromRGBO(13, 48, 47, 1),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10,),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            deleteUser(uid);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                            color: Colors.lightBlueAccent,
                              borderRadius: BorderRadius.circular(10),

                            ),
                            padding: EdgeInsets.all(8),
                            child: Text(
                              'YES',
                              textAlign: TextAlign.center,

                              style: TextStyle(
                                color: Colors.white
                                // color: Color.fromRGBO(49, 105, 226,
                                    // 1), // Set the text color to black
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }
}
