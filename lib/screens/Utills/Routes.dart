import 'package:get/get.dart';
import 'package:project_name/screens/Delete_team/delete_team_binding.dart';
import 'package:project_name/screens/Delete_team/delete_team_screen.dart';
import 'package:project_name/screens/Delete_user/Delete_users_binding.dart';
import 'package:project_name/screens/Private_Screen/UserProfile/userprofile_binding.dart';
import 'package:project_name/screens/Private_Screen/UserProfile/userprofile_screen.dart';
import 'package:project_name/screens/Public_Screen/GroupsDetail/groupsdetail_binding.dart';
import 'package:project_name/screens/Public_Screen/Live_Update/liveupdate_binding.dart';
import 'package:project_name/screens/Public_Screen/Live_Update/liveupdate_screen.dart';
import 'package:project_name/screens/Public_Screen/ScoreBook/Scorebook_binding.dart';
import 'package:project_name/screens/Public_Screen/ScoreBook/Scorebook_screen.dart';
import 'package:project_name/screens/create_team/create_team_binding.dart';
import 'package:project_name/screens/create_team/create_team_screen.dart';
import 'package:project_name/screens/create_user/create_user_binding.dart';
import 'package:project_name/screens/create_user/create_user_screen.dart';
import '../Delete_user/Delete_users_screen.dart';
import '../Public_Screen/Group/group_binding.dart';
import '../Public_Screen/Group/group_screen.dart';
import '../Public_Screen/GroupsDetail/groupdetail_Screen.dart';
import '../Public_Screen/NewMatch/Newmatch_biinding.dart';
import '../Public_Screen/NewMatch/Newmtach_screen.dart';
import '../Public_Screen/Points-Talble/pointstable_binding.dart';
import '../Public_Screen/Points-Talble/pointstable_screen.dart';
import '../Public_Screen/ScoreCard007/scorecard007_binding.dart';
import '../Public_Screen/ScoreCard007/scorecard007_screen.dart';
import '../Public_Screen/SplashScreen/Splash_binding.dart';
import '../Public_Screen/SplashScreen/Splash_screen.dart';
import '../Public_Screen/Start Match/Startmatch_binding.dart';
import '../Public_Screen/Start Match/Startmatch_screen.dart';
import '../Public_Screen/dashboardcricket/dashboardcricket_binding.dart';
import '../Public_Screen/dashboardcricket/dashboardcricket_screen.dart';
import '../Public_Screen/ranking/ranking_binding.dart';
import '../Public_Screen/ranking/ranking_screen.dart';
import '../Public_Screen/login/login_bindimg.dart';
import '../Public_Screen/login/login_screen.dart';
import '../Private_Screen/mainprofile/mainprofile_binding.dart';
import '../Private_Screen/mainprofile/mainprofile_screen.dart';
import '../Public_Screen/profile/profile_binding.dart';
import '../Public_Screen/profile/profile_screen.dart';
import '../Private_Screen/setting/setting_binding.dart';
import '../Private_Screen/setting/setting_screen.dart';
import '../state/state_binding.dart';
import '../state/state_screen.dart';
import 'Paths.dart';

class Routes {
  static final routes = [
    GetPage(
      name: Paths.SplashScreen,
      page: () => SplashScreen(),
      binding: Splashbinding(),
    ),
    GetPage(
      name: Paths.GroupScreen,
      page: () => GroupScreen(),
      binding: Groupbinding(),
    ),
    GetPage(
      name: Paths.Scorebookscreen,
      page: () => ScoreBookScreen(),
      binding: ScoreBookBinding(),
    ),
    GetPage(
      name: Paths.StartmatchScreen,
      page: () => StartmatchScreen(),
      binding: StartmatchBinding(),
    ),
    GetPage(
      name: Paths.dashboardcricketScreen,
      page: () => DashboardcricketScreen(),
      binding: DashboardcricketBinding(),
    ),
    GetPage(
      name: Paths.ScorecardScreen,
      page: () => ScoreCardScreen(),
      binding: ScorecardBinding(),
    ),
    GetPage(
      name: Paths.PointstableScreen,
      page: () => PointstableScreen(),
      binding: PointstableBinding(),
    ),
    GetPage(
      name: Paths.NewMatchScreen,
      page: () => NewMatchScreen(),
      binding: NewMatchdBinding(),
    ),
    GetPage(
      name: Paths.LiveupdateScreen,
      page: () => LiveupdateScreen(),
      binding: LiveupdateBinding(),
    ),
    GetPage(
      name: Paths.RankingScreen,
      page: () => RankingScreen(),
      binding: RankingBinding(),
    ),
    GetPage(
      name: Paths.StateScreen,
      page: () => StateScreen(),
      binding: StateBinding(),
    ),
    GetPage(
      name: Paths.SettingScreen,
      page: () => SettingScreen(),
      binding: SettingBinding(),
    ),
    GetPage(
      name: Paths.LoginEmailScreen,
      page: () => LoginEmailScreen(),
      binding: LoginEmailBinding(),
    ),
    GetPage(
      name: Paths.GroupdetailScreen,
      page: () => GroupdetailScreen(),
      binding: GroupdetailBinding(),
    ),
    GetPage(
      name: Paths.MyProfileScreen,
      page: () => MyProfileScreen(),
      binding: MyProfileBinding(),
    ),
        GetPage(
      name: Paths.MainProfileScreen,
      page: () => MainProfileScreen(),
      binding: MainProfileBinding(),
    ),
      GetPage(
      name: Paths.userProfileScreen,
      page: () => userProfileScreen(),
      binding: UserProfileBinding(),
    ),
    GetPage(
      name: Paths.createUserScreen,
      page: () => CreatUserScreen(),
      binding: CreatUserBinding(),
    ),
     GetPage(
      name: Paths.deleteUsersScreen,
      page: () => DeleteUsersScreen(),
      binding: DeleteUsersbinding(),
    ),
    GetPage(
      name: Paths.createteamScreen,
      page: () => CreateteamScreen(),
      binding: CreateteamBinding(),
    ),
     GetPage(
      name: Paths.deleteteamScreen,
      page: () => DeleteteamScreen(),
      binding: Deleteteambinding(),
    ),
  ];

}
