import 'package:get/get.dart';
import 'http_service.dart';
import 'storage_service.dart';
import 'util_service.dart';

// import 'walletconnect.service.dart';

initializeServices() async {
  await Get.putAsync(() => StorageService().init());
  await Get.putAsync(() => UtilService().init());
  await Get.putAsync(() => HTTPService().init());
  
}
