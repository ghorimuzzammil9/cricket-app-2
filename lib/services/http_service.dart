import 'package:get/get.dart';
import 'package:project_name/services/storage_service.dart';
import 'package:project_name/services/util_service.dart';


class HTTPService extends GetConnect {
  // UserController userController = Get.find<UserController>();
  StorageService storage = Get.find<StorageService>();
  UtilService util = Get.find<UtilService>();

  Future<HTTPService> init() async {
    httpClient.timeout = Duration(seconds: 60);
    timeout = Duration(seconds: 60);
    httpClient.addRequestModifier<dynamic>((request) async {
      dynamic header = await getRequestHeader();
      if (header != null && header.length > 0) {
        request.headers.addAll(header);
      }
      return request;
    });
    httpClient.addResponseModifier<dynamic>((request, response) async {
      if (response.statusCode == 401) {
        if (storage.getUser()!.uid != "") {
          Get.snackbar("Error!", "Token Expired Please Login Again");
          // FBroadcast.instance().broadcast("logout");
        }
      }
      return response;
    });
    return this;
  }

  Future<Map<String, String>> getRequestHeader() {
    dynamic token = storage.getData("token");
    var user = storage.getUser();
    Map<String, String> headers = {};
    if (token != null) {
      headers = {
        'Authorization': "Bearer $token",
      };
      if (user!.uid != "" && user.uid != null) {
        headers = {
          'Authorization': "Firebase $token",
        };
      }
    }
    return Future.value(headers);
  }


  
  
}
