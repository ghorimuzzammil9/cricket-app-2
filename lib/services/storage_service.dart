// ignore_for_file: unused_local_variable

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'dart:convert';

import 'package:project_name/model/user_model.dart';

import '../../screens/Utills/Paths.dart';
import '../../screens/Utills/storage_keys.dart';

class StorageService extends GetxService {
  GetStorage storage = GetStorage();

  Future<StorageService> init() async {
    await GetStorage.init();
    await 1.delay();
    return this;
  }

  bool isAuthenticated() {
    var token = getData(StorageKeys.token);
    return token != null ? true : false;
  }

  getData(String key) {
    var data = storage.read(key);
    if (data != null) {
      return json.decode(data.toString());
    }
    return data;
  }

  setData(String key, dynamic data) {
    var encodedData = json.encode(data);
    storage.write(key, encodedData);
  }

  setInMemoryData(String key, dynamic data) {
    var encodedData = json.encode(data);
    storage.writeInMemory(key, encodedData);
  }

  removeAllProperties() {
    storage.erase();
  }

  removeKey(String key) {
    storage.remove(key);
  }

  void setUser(UserModel newUser) {
    var user = newUser;
    setData("user", newUser.toJson());
    Get.forceAppUpdate();
  }

  UserModel? getUser() {
    var data = getData("user");
    if (data != null) {
      return UserModel.fromJson(data);
    }
    return null;
  }

  void logout() {
    removeAllProperties();
    Get.offAllNamed(Paths.LoginEmailScreen, predicate: (route) => false);
  }

  getDeviceId() {
    var deviceId = this.getData(StorageKeys.deviceId);
    if (deviceId != null) {
      return deviceId;
    } else {
      deviceId = DateTime.now().millisecondsSinceEpoch.toString();
      this.setData(StorageKeys.deviceId, deviceId);
      return deviceId;
    }
  }
}
