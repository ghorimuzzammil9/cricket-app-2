// ignore_for_file: unused_field, unused_local_variable

import 'package:get/get.dart';

import '../../model/user_model.dart';
import '../../screens/Utills/Paths.dart';
import '../../services/http_service.dart';
import '../../services/storage_service.dart';
import '../../services/util_service.dart';

class UserController extends GetxController {
  UserModel? user;
  StorageService storageService = Get.find<StorageService>();
  HTTPService http = Get.find<HTTPService>();
  UtilService util = Get.find<UtilService>();
  bool _serviceEnabled = false;
  dynamic currencyRate;
  String currency = "AED";
  get addressDTO => null;

  @override
  void onInit() {
    super.onInit();
    getUser();
    
    // getLocation();
  }

  removeAllProperties() {
    user = null;

    update();
  }

  
  void setUser(UserModel? newUser) {
    // if (user != null && user!.addresses != null) {
    //   user!.addresses!.sort(
    //     (a, b) => a.id!.compareTo(b.id!),
    //   );
    // }
    user = newUser;
    storageService.setData("user", newUser!.toJson());

    update();
  }



  // void setCurrency(UserModel newUser) {
  //   // user = newUser;
  //   // storageService.setData("user", newUser.toJson());

  //   update();
  // }

  getUser() {
    var data = storageService.getData("user");
    if (data != null) {
      user = UserModel.fromJson(data);
    }
  }

  bool get isUser {
    if (user != null) {
      return user!.type == 'user';
    }
    return true;
  }

 




  goToProfileScreen(String postUserId, String postOwnerId,
      String postProfilePicture, String postFullName) {
    Get.toNamed(
      Paths.MyProfileScreen,
      arguments: {
        "postUserId": postUserId,
        "postOwnerId": postOwnerId,
        "postProfilePicture": postProfilePicture,
        "postFullName": postFullName,
      },
      parameters: {
        "id": postUserId,
      },
      preventDuplicates: false,
    );
    update();
  }



 

  // showAlert({
  //   String message = "You want to delete this account",
  //   String title = "Are you sure?",
  //   String confirmText = "Yes",
  //   bool isSingleButton = false,
  //   bool backgroundDismissal = false,
  // }) {
  //   Get.defaultDialog(
  //     barrierDismissible: backgroundDismissal,
  //     title: title,
  //     titleStyle: TextStyle(
  //       fontSize: 18,
  //       fontWeight: FontWeight.w600,
  //       fontFamily: "Poppins",
  //       color: Color.fromRGBO(42, 46, 67, 1),
  //     ),
  //     contentPadding: EdgeInsets.only(left: 15, right: 15, top: 15),
  //     middleText: message,
  //     middleTextStyle: TextStyle(
  //       fontSize: 13,
  //       fontFamily: "Poppins",

  //       fontWeight: FontWeight.w500,
  //       color: Color.fromRGBO(42, 46, 67, 1),
  //       // height: 1.5,
  //     ),
  //     radius: 12,
  //     titlePadding: EdgeInsets.only(top: 25),
  //     actions: [
  //       Container(
  //           width: Get.width,
  //           margin: EdgeInsets.only(top: 0),
  //           child: Row(
  //             mainAxisAlignment: MainAxisAlignment.spaceAround,
  //             children: [
  //               Container(
  //                 margin: EdgeInsets.only(top: 10, bottom: 20),
  //                 width: Get.width * .30,
  //                 height: 40,
  //                 child: ElevatedButton(
  //                   onPressed: () {
  //                     deleteAccount();
  //                   },
  //                   child: Text(
  //                     "user_yes".tr,
  //                     style: TextStyle(
  //                         fontSize: 16,
  //                         fontFamily: "Poppins",
  //                         color: Colors.white,
  //                         fontWeight: FontWeight.w500),
  //                     textAlign: TextAlign.center,
  //                   ),
  //                   style: ElevatedButton.styleFrom(
  //                     backgroundColor: Get.theme.primaryColor,
  //                     elevation: 0,
  //                     shape: RoundedRectangleBorder(
  //                         borderRadius: BorderRadius.circular(10)),
  //                   ),
  //                 ),
  //               ),
  //               Container(
  //                 margin: EdgeInsets.only(top: 10, bottom: 20),
  //                 width: Get.width * .30,
  //                 height: 40,
  //                 child: ElevatedButton(
  //                   onPressed: () {
  //                     Get.back();
  //                   },
  //                   child: Text(
  //                     "user_no".tr,
  //                     style: TextStyle(
  //                         fontSize: 16,
  //                         fontFamily: "Poppins",
  //                         color: Colors.white,
  //                         fontWeight: FontWeight.w500),
  //                     textAlign: TextAlign.center,
  //                   ),
  //                   style: ElevatedButton.styleFrom(
  //                     backgroundColor: Get.theme.hintColor,
  //                     elevation: 0,
  //                     shape: RoundedRectangleBorder(
  //                         borderRadius: BorderRadius.circular(10)),
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           )),
  //     ],
  //   );
  // }



}
