import 'package:get/get.dart';
import 'package:project_name/controller/controllers/user_controller.dart';
// import 'package:ivobs_app/controllers/create_post_controller.dart';

class InitialBindings extends Bindings {
  @override
  void dependencies() {
    Get.put(UserController());
    // Get.put(MyCartController());
    // Get.put(BookingServiceController());
    // Get.put(LogoutController());
    // Get.put(AuctionItemController());
  }
}
